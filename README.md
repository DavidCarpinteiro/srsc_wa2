## How to run with docker

First thing is to delete the folder "out/production/proj2".
After that launch Intellij and click "Build"

There are two scrips "create.sh" and "run.sh".

The script "create.sh" is used to build the images and setup the containers. It also creates the required network.

The script "run.sh" is used to run the containers, it tries to kill all running containers (will produce error messages if none are running) and then starts them in order.

It can run all containers in detached mode (no output) except for the "dispatcher" whose output will be showed. (run this if the other does not work)

It can also run all containers in different windows, with the output of each one.
 
Once executed it will ask which one to run. 

After all that, just run the "ClientLauncher" inside Intellij.