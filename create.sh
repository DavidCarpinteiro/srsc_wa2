#!/bin/bash

docker rm -f auth
docker rm -f acesscontrol
docker rm -f file
docker rm -f dispatcher

# Intellij does not copy empty folders to output folder
# need to do it manually
cp -r ./tables ./out/production/proj2/

docker network create srscnet

docker build -t auth -f ./dockers/auth/Dockerfile .
docker build -t acesscontrol -f ./dockers/accessControl//Dockerfile .
docker build -t file -f ./dockers/file/Dockerfile .
docker build -t dispatcher -f ./dockers/dispatcher/Dockerfile .

docker create --name auth --net srscnet --ip 192.168.0.5 auth
docker create --name acesscontrol --net srscnet --ip 192.168.0.6 acesscontrol
docker create --name file --net srscnet --ip 192.168.0.7 file
docker create --name dispatcher --net srscnet --ip 192.168.0.8 -p 443:443 dispatcher