#!/bin/bash

echo "Do not press any key until the end"

# Clear old files
#rm -v !("keys.sh")
echo "Clearing old files"
find . -type f -not -name '*sh' -print0 | xargs -0 rm --

createKeyStore () {
    keytool -genkey -alias ${1} -keyalg ${4} -keystore ${2}.ks -keypass ${3} -storepass ${3} -validity 360 -keysize ${5} -dname "CN=0.0.0.0, OU=Example, O=Example Company, L=Example, ST=Lisbon, C=PT"
}

createCert() {
    keytool -export -keystore ${2}.ks -alias ${1} -file ${4}.cer -storepass ${3}
}

createTrustStore() {
    yes | keytool -importcert -alias ${3} -file ${4}.cer -keystore ${1}.ts -storepass ${2}
}

echo "Generating Keystores"

createKeyStore dispatcher dispatcher dispatcher RSA 2048
createKeyStore client client client RSA 2048
createKeyStore authenticator authenticator authenticator RSA 2048
createKeyStore access access access RSA 2048
createKeyStore storage storage storage RSA 2048

createKeyStore dispatcher_dsa dispatcher dispatcher EC 256
createKeyStore client_dsa client client EC 256
createKeyStore authenticator_dsa authenticator authenticator EC 256
createKeyStore access_dsa access access EC 256
createKeyStore storage_dsa storage storage EC 256

echo "Generating Certificates"

createCert dispatcher dispatcher dispatcher dispatcher
createCert client client client client
createCert authenticator authenticator authenticator authenticator
createCert access access access access
createCert storage storage storage storage

createCert dispatcher_dsa dispatcher dispatcher dispatcher_dsa
createCert client_dsa client client client_dsa
createCert authenticator_dsa authenticator authenticator authenticator_dsa
createCert access_dsa access access access_dsa
createCert storage_dsa storage storage storage_dsa

echo "Generating Truststores"

createTrustStore dispatcher dispatcher client client
createTrustStore dispatcher dispatcher authenticator authenticator
createTrustStore dispatcher dispatcher access access
createTrustStore dispatcher dispatcher storage storage

createTrustStore dispatcher dispatcher client_dsa client_dsa
createTrustStore dispatcher dispatcher authenticator_dsa authenticator_dsa
createTrustStore dispatcher dispatcher access_dsa access_dsa
createTrustStore dispatcher dispatcher storage_dsa storage_dsa

createTrustStore client client dispatcher dispatcher
createTrustStore client client dispatcher_dsa dispatcher_dsa

createTrustStore authenticator authenticator dispatcher dispatcher
createTrustStore authenticator authenticator dispatcher_dsa dispatcher_dsa

createTrustStore access access dispatcher dispatcher
createTrustStore access access authenticator authenticator
createTrustStore access access dispatcher_dsa dispatcher_dsa
createTrustStore access access authenticator_dsa authenticator_dsa

createTrustStore storage storage dispatcher dispatcher
createTrustStore storage storage access access
createTrustStore storage storage dispatcher_dsa dispatcher_dsa
createTrustStore storage storage access_dsa access_dsa

# Remove Certificates
echo "Removing Certificates"
find . -type f -name '*.cer' -delete

echo "Done!"