package utils;

import exceptions.EncryptionError;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.io.*;
import java.nio.ByteBuffer;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Random;

public class CipherSuite {

    public static PublicKey getKey(String key) {
        try {
            byte[] byteKey = toBytes(key);
            X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(byteKey);
            KeyFactory kf = KeyFactory.getInstance("DH");
            return kf.generatePublic(X509publicKey);
        } catch (Exception e) {
            // e.printStackTrace();
            throw new RuntimeException("Could not Get Key");
        }
    }

    public static String getString(PublicKey key) {
        try {
            byte[] bytes = key.getEncoded();
            return toBase64(bytes);
        } catch (Exception e) {
            //e.printStackTrace();
            throw new RuntimeException("Could not Convert Key");
        }
    }


    public static String encryptPBE(String value, String password, long salt_l) {
        byte[] plaintext = toBytes(value);
        PBEParameterSpec pbeParamSpec = createSpec(salt_l);
        PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray());

        try {
            SecretKeyFactory kf = SecretKeyFactory.getInstance("PBEWithHmacSHA256AndAES_128");
            SecretKey secretKey = kf.generateSecret(keySpec);

            Cipher enc = Cipher.getInstance("PBEWithHmacSHA256AndAES_128");
            enc.init(Cipher.ENCRYPT_MODE, secretKey, pbeParamSpec);

            return toBase64(enc.doFinal(plaintext));
        } catch (InvalidKeySpecException | NoSuchAlgorithmException | BadPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | NoSuchPaddingException | IllegalBlockSizeException e) {
            //e.printStackTrace();
            throw new RuntimeException("Could not Encrypt PBE");
        }
    }

    public static String decryptPBE(String value, String password, long salt_l) {
        byte[] plaintext = toBytes(value);
        PBEParameterSpec pbeParamSpec = createSpec(salt_l);
        PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray());

        try {
            SecretKeyFactory kf = SecretKeyFactory.getInstance("PBEWithHmacSHA256AndAES_128");
            SecretKey secretKey = kf.generateSecret(keySpec);

            Cipher enc = Cipher.getInstance("PBEWithHmacSHA256AndAES_128");
            enc.init(Cipher.DECRYPT_MODE, secretKey, pbeParamSpec);

            return toBase64(enc.doFinal(plaintext));
        } catch (InvalidKeySpecException | NoSuchAlgorithmException | BadPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | NoSuchPaddingException | IllegalBlockSizeException e) {
            //e.printStackTrace();
            throw new RuntimeException("Could not Encrypt PBE");
        }
    }

    private static PBEParameterSpec createSpec(long salt_l) {
        byte[] salt = ByteBuffer.allocate(Long.SIZE / Byte.SIZE).putLong(salt_l).array();
        Random rnd = new Random(salt_l);

        byte[] iv = new byte[16];
        rnd.nextBytes(iv);

        IvParameterSpec ivParamSpec = new IvParameterSpec(iv);
        return new PBEParameterSpec(salt, 10000, ivParamSpec);
    }

    public static String encryptAsymmetric(String value, Key key) throws EncryptionError {
        try {
            Cipher cipher = Cipher.getInstance(Conf.get(Props.ASYMMETRIC_TYPE));
            cipher.init(Cipher.ENCRYPT_MODE, key);

            return toBase64(cipher.doFinal(toBytes(value)));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            //e.printStackTrace();
            throw new EncryptionError("Could not Encrypt RSA");
        }
    }

    public static String decryptAsymmetric(String value, Key key) throws EncryptionError {
        try {
            Cipher cipher = Cipher.getInstance(Conf.get(Props.ASYMMETRIC_TYPE));
            cipher.init(Cipher.DECRYPT_MODE, key);

            return toBase64(cipher.doFinal(toBytes(value)));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            //e.printStackTrace();
            throw new EncryptionError("Could not Decrypt RSA");
        }
    }

    public static String digest(String value) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            return toBase64(messageDigest.digest(toBytes(value)));
        } catch (NoSuchAlgorithmException e) {
            //e.printStackTrace();
            throw new RuntimeException("Could not Digest");
        }
    }


    public static String toBase64(byte[] bytes) {
        return Base64.getEncoder().encodeToString(bytes);

    }

    public static byte[] toBytes(String data) {
        return Base64.getDecoder().decode(data);
    }

    public static long getRandom() {
        return new SecureRandom().nextLong() - 1;
    }


    public static String serialize(Object obj) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(obj);
        return toBase64(out.toByteArray());
    }

    public static Object deserialize(String data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(toBytes(data));
        ObjectInputStream is = new ObjectInputStream(in);
        return is.readObject();
    }

    public static KeyPair loadKeys(String keystore_path, String password, String alias) {
        try {
            FileInputStream is = new FileInputStream(keystore_path);

            KeyStore keystore = KeyStore.getInstance(Conf.get(Props.KEYSTORE_TYPE));
            keystore.load(is, password.toCharArray());

            Key key = keystore.getKey(alias, password.toCharArray());
            if (key instanceof PrivateKey) {
                java.security.cert.Certificate cert = keystore.getCertificate(alias);

                PublicKey publicKey = cert.getPublicKey();

                return new KeyPair(publicKey, (PrivateKey) key);
            }

        } catch (IOException | CertificateException | UnrecoverableKeyException | NoSuchAlgorithmException | KeyStoreException e) {
            e.printStackTrace();
            throw new RuntimeException("Error loading keys");
        }

        throw new RuntimeException("No Private Key Found");
    }

    public static PublicKey loadPublicKey(String truststore, String password, String alias) {
        try {
            FileInputStream is = new FileInputStream(truststore);

            KeyStore keystore = KeyStore.getInstance(Conf.get(Props.TRUSTSTORE_TYPE));
            keystore.load(is, password.toCharArray());

            Certificate c = keystore.getCertificate(alias);

            return c.getPublicKey();
        } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
            e.printStackTrace();
            throw new RuntimeException("could not extract public key from truststore");
        }
    }


}
