package utils;

import exceptions.EncryptionError;

import javax.crypto.*;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;

public class DiffieHellman {

    private KeyAgreement agreement;
    private KeyPair pair;
    private byte[] sharedSecret;

    //Server
    public DiffieHellman() {
        try {
            KeyPairGenerator pair_gen = KeyPairGenerator.getInstance("DH");
            pair_gen.initialize(2048);
            pair = pair_gen.generateKeyPair();

            agreement = KeyAgreement.getInstance("DH");
            agreement.init(pair.getPrivate());
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed at Diffie Init");
        }

    }

    //Client
    public DiffieHellman(Key publicKey) {
        DHParameterSpec dhParamFromAlicePubKey = ((DHPublicKey) publicKey).getParams();

        try {
            KeyPairGenerator pair_gen = KeyPairGenerator.getInstance("DH");
            pair_gen.initialize(dhParamFromAlicePubKey);
            pair = pair_gen.generateKeyPair();

            agreement = KeyAgreement.getInstance("DH");
            agreement.init(pair.getPrivate());

            agreement.doPhase(publicKey, true);

            sharedSecret = new byte[256];
            agreement.generateSecret(sharedSecret, 0);
        } catch (InvalidKeyException | NoSuchAlgorithmException | InvalidAlgorithmParameterException | ShortBufferException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed at Diffie Init");
        }
    }

    // Server
    public void doFinal(Key publicKey) {
        try {
            agreement.doPhase(publicKey, true);
            sharedSecret = agreement.generateSecret();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed at Diffie Final");
        }
    }

    public PublicKey getPublic() {
        return pair.getPublic();
    }

    public Key getPrivate() {
        return pair.getPrivate();
    }

    public String encrypt(String value) throws EncryptionError {
        SecretKeySpec aesKey = new SecretKeySpec(sharedSecret, 0, 16, "AES");

        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);

            return CipherSuite.toBase64(cipher.doFinal(CipherSuite.toBytes(value)));
        } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new EncryptionError();
        }

    }

    public String decrypt(String value) throws EncryptionError {
        SecretKeySpec aesKey = new SecretKeySpec(sharedSecret, 0, 16, "AES");

        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, aesKey);

            return CipherSuite.toBase64(cipher.doFinal(CipherSuite.toBytes(value)));
        } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new EncryptionError();
        }
    }
}
