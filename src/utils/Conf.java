package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

public class Conf {

    private static final Map<Props, String> props = new HashMap<>(20);

    private static final String SEPARATOR = ";";

    public static void newInstance(String conf_file_path) {
        load(conf_file_path);
    }

    public static String get(Props p) {
        Objects.requireNonNull(p);

        if (props.isEmpty()) throw new RuntimeException("properties not loaded");

        String tmp = props.get(p);

        if (tmp == null) throw new RuntimeException("no property found for: " + p.name());

        return tmp;
    }

    public static String[] getAll(Props p) {
        String tmp = get(p);

        return tmp.split(SEPARATOR);
    }

    private static void load(String location) {
        Properties properties = new Properties();

        try {
            properties.load(new FileInputStream(location));

            for (Props p : Props.values()) {
                String tmp = (String) properties.get(p.name());

                if (tmp == null || tmp.isEmpty()) {
                    throw new RuntimeException("incomplete properties file: " + location);
                }

                props.put(p, tmp);
            }

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("error reading properties file: " + location);
        }
    }

    public enum TYPE {
        CLIENT, SERVER
    }
}
