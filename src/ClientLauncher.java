import communication.channel.Channel;
import communication.channel.ClientCom;
import communication.message.Package;
import exceptions.EncryptionError;
import principals.client.Client;
import principals.client.FileManager;
import principals.client.TestClient;
import utils.Conf;

import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.InvalidPathException;
import java.util.Collections;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

public class ClientLauncher {

    public static void main(String[] args) {
        //System.setProperty("javax.net.debug", "ssl");

        System.out.println("Client running...");

        String keystore = "configs/tls/client.ks";
        String password = "client";
        String truststore = "configs/tls/client.ts";

        String ip = "192.168.0.8";
        int port = 4443;

        String config_path = "configs/other/client.properties";

        if (args.length == 6) {
            try {
                port = Integer.parseInt(args[0]);
                ip = args[1];
                keystore = args[2];
                password = args[3];
                truststore = args[4];
                config_path = args[5];
            } catch (NumberFormatException ignored) {

            }
        }

        System.setProperty("javax.net.ssl.trustStorePassword", password);
        System.setProperty("javax.net.ssl.trustStore", truststore);

        Conf.newInstance(config_path);

        Channel<Package> channel = ClientCom.getChannel(keystore, password, ip, port);

        // TODO change to load crypto configs and block size from file?
        FileManager fileManager = new FileManager(keystore, password, "client", "AES/ECB/PKCS5Padding", "AES", 16);
        Client c = new Client("test", channel, fileManager);

        try {
            // Token with valid auth signature, but its date is May 23rd;
            String invalidTokenTTL = "YKpgqrWj4oVlbTNCL4lqcyjpILN3YnKJmbiJDfcKn6SkFK+R7V05TFB+tUyZNIu4+nt8rMI4lmVKzHN/FKHC0u/DUDG4g/tMBSe+SaESuVtxuhJWfBaH2IHap9Dq3biXzoIcdSRTmPW3PWy7q8HGlx45ofs4hQUoQBWVA+TKe+mZmBSnY08d73/7p7TIEOpEryKMzzIfzjm7xFvoYZRQSjUttrQN5kfwWEBM6RRoUAI8n52K/U0lXJp9Jqyh0qgO/oaedu0rhMxyIGpZxIbHytg5sKQ/8lqG75Ytux0+1UEjvkV9tWxlPkAti3LdH7o5e00mGtZ7lgXCBLLl3r5/Xw==";
            testInvalidToken(keystore, password, channel, invalidTokenTTL);

            // Token with valid date, but invalid signature;
            String invalidTokenSignature = "iqSG9CaFgwDawO4NdNmZngRFWaCWEebbm+da6txOoi4+y+OVJNt8iCJ/jgXIjRpMSlNGJxYUb/RISaOQlXSI6OD+wqLbnsXUFwUWfzZ/iP3l597ZxvoINIojKcNufQhlFUGDTIvugFgJ1ndNaOqdWa/b1ImfBN5nAnbfAVA3fwkHwS1gkmTi5hlDAnPb4Yh/XSRMznGB35d+W6oZ8pLqcuLJdDH96gsmYUTxL6wAnxmCGKcJED8Txp5LBrPhxMDVRj62L6i4FWXjnZKPV+WMSBqo+gBqNLrHizM1yRyGyEC5cBJ8PsCHW5ABNiMQQaxuqfsNyYGVp1sLnddaAk6JFA==";
            testInvalidToken(keystore, password, channel, invalidTokenSignature);

            permissionsTest(keystore, password, channel);
            test(c);
        } catch (EncryptionError encryptionError) {
            encryptionError.printStackTrace();
        }
    }

    public static void test(Client c) throws EncryptionError {

        assertTrue(c.login("test"));

        assertTrue(c.mkdir("/boat"));
        assertFalse(c.mkdir("/boat"));
        try {
            assertTrue(c.file("/boat").isDirectory());
        } catch (FileNotFoundException e) {
            fail("DIR IS NOT DIR");
        }
        assertTrue(c.rmdir("/boat"));

        byte[] parrot = "Hello, World! :) There and Back Again".getBytes(); // Three block write test; for blocksize = 16;
        assertFalse(c.rm("/parrot"));
        try {
            assertTrue(c.put("/parrot", parrot));
        } catch (FileAlreadyExistsException e) {
            fail("FILE ALREADY EXISTS");
        }
        try {
            assertFalse(c.file("/parrot").isDirectory());
        } catch (FileNotFoundException e) {
            fail("FIEL IS DIR");
        }
        byte[] res = null;
        try {
            assertArrayEquals(parrot, res = c.get("/parrot"));
            System.out.println(new String(res));
        } catch (FileNotFoundException e) {
            fail("COULD NOT GET FILE");
        }
        assertTrue(c.rm("/parrot"));

        assertEquals(new LinkedList<>(), c.ls());
        try {
            assertEquals(new LinkedList<>(), c.ls("/dir1"));
            fail("InvalidPathException not thrown");
        } catch (InvalidPathException ignore) {
        }
        assertTrue(c.mkdir("/dir1"));
        assertEquals(Collections.singletonList("dir1"), c.ls());
        assertEquals(new LinkedList<String>(), c.ls("/dir1"));

        byte[] file = new byte[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        try {
            assertTrue(c.put("/dir1/file", file));
        } catch (FileAlreadyExistsException e) {
            fail("FILE ALREADY EXISTS");
        }

        try {
            assertArrayEquals(file, c.get("/dir1/file"));
            assertThrows(FileNotFoundException.class, () -> c.get("/dir1/file2"));
        } catch (FileNotFoundException e) {
            fail("FILE NOT FOUND: " + e.getMessage());
        }

        try {
            assertNotNull(c.file("/dir1/file"));
            assertFalse(c.file("/dir1/file").isDirectory());
            assertEquals("file", c.file("/dir1/file").getName());
            assertEquals("file", c.file("/dir1/file").getType());

            assertTrue(c.file("/dir1").isDirectory());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        assertEquals(Collections.singletonList("file"), c.ls("/dir1"));

        assertTrue(c.mkdir("/dir1/dir2"));
        assertFalse(c.mkdir("/dir1/dir2"));
        assertTrue(c.cp("/dir1/file", "/dir1/dir2/file1"));

        try {
            assertArrayEquals(c.get("/dir1/dir2/file1"), file);
            assertThrows(FileNotFoundException.class, () -> c.get("/dir1/file2"));
        } catch (FileNotFoundException ignore) {
        }

        c.ls();
        c.ls("/dir1");
        c.ls("/dir1/dir2");

        assertFalse(c.rm("/dir1/dir2/file2"));
        assertFalse(c.rm("/dir1/dir2"));
        assertTrue(c.rm("/dir1/dir2/file1"));

        assertFalse(c.rmdir("/dir1/dir2/file1"));
        assertFalse(c.rmdir("/dir1/dir3"));
        assertTrue(c.rm("/dir1/file"));


        assertThrows(FileNotFoundException.class, () -> c.file("/dir1/file2"));
        assertThrows(FileNotFoundException.class, () -> c.file("/dir1/file"));

        assertTrue(c.rmdir("/dir1/dir2"));
        assertTrue(c.rmdir("/dir1"));

        try {
            assertTrue(c.put("/file.txt", new byte[]{0, 1, 2, 3, 4}));
        } catch (FileAlreadyExistsException e) {
            fail("file.txt already exists");
        }

        try {
            assertEquals("txt", c.file("/file.txt").getType());
        } catch (FileNotFoundException e) {
            fail("file.txt not found");
        }

        assertFalse(c.rm("/file"));
        assertTrue(c.rm("/file.txt"));

        System.out.println("TEST FINISHED SUCCESSFULLY");
    }


    private static void permissionsTest(String keystore, String password, Channel<Package> channel) throws EncryptionError {
        FileManager fileManager = new FileManager(keystore, password, "client", "AES/ECB/PKCS5Padding", "AES", 48);
        Client readUser = new Client("readUser", channel, fileManager);

        assertTrue(readUser.login("test"));
        assertNotNull(readUser.ls());
        assertFalse(readUser.mkdir("/dir1"));

        fileManager = new FileManager(keystore, password, "client", "AES/ECB/PKCS5Padding", "AES", 48);
        Client test = new Client("test", channel, fileManager);


        System.out.println("PERMISSIONS TEST COMPLETED");
    }

    private static void testInvalidToken(String keystore, String password, Channel<Package> channel, String invalidToken) throws EncryptionError {
        FileManager fileManager = new FileManager(keystore, password, "client", "AES/ECB/PKCS5Padding", "AES", 48);
        Client c = new TestClient("test", channel, fileManager, invalidToken);

        assertTrue(c.login("test"));

        assertFalse(c.mkdir("/dir1"));

        System.out.println("INVALID TOKEN TEST PASSED SUCCESSFULLY");

    }
}
