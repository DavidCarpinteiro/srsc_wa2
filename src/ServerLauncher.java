import communication.channel.Channel;
import communication.channel.ClientCom;
import communication.channel.ServerCom;
import communication.handler.Handler;
import communication.message.Package;
import exceptions.NoConnection;
import principals.server.main_dispatcher.ClientHandler;
import principals.server.main_dispatcher.MainDispatcher;
import utils.CipherSuite;
import utils.Conf;

import java.security.PublicKey;

public class ServerLauncher {

    public static void main(String[] args) {
        //System.setProperty("javax.net.debug", "ssl");

        System.out.println("Server Dispatcher running...");

        String keystore = "configs/tls/dispatcher.ks";
        String password = "dispatcher";
        String truststore = "configs/tls/dispatcher.ts";
        String alias_of_other = "authenticator";

        int port = 4443;

        //Setup Connections to Other Servers
        String ip_auth = "localhost";
        int port_auth = 1111;
        String ip_access = "localhost";
        int port_access = 2222;
        String ip_file = "localhost";
        int port_file = 3333;
        String config_path = "configs/other/server.properties";

        if (args.length == 12) {
            try {
                port = Integer.parseInt(args[0]);
                keystore = args[1];
                password = args[2];
                truststore = args[3];

                ip_auth = args[4];
                port_auth = Integer.parseInt(args[5]);
                ip_access = args[6];
                port_access = Integer.parseInt(args[7]);

                ip_file = args[8];
                port_file = Integer.parseInt(args[9]);

                alias_of_other = args[10];
                config_path = args[11];
            } catch (NumberFormatException ignored) {

            }
        }

        System.setProperty("javax.net.ssl.trustStore", truststore);
        System.setProperty("javax.net.ssl.trustStorePassword", password);

        Conf.newInstance(config_path);
        PublicKey key = CipherSuite.loadPublicKey(truststore, password, alias_of_other);
        MainDispatcher.setPublicKey(key);

        Channel<Package> channel_auth = ClientCom.getChannel(keystore, password, ip_auth, port_auth);
        Channel<Package> channel_access = ClientCom.getChannel(keystore, password, ip_access, port_access);
        Channel<Package> channel_file = ClientCom.getChannel(keystore, password, ip_file, port_file);

        MainDispatcher.setAuthChannel(channel_auth);
        MainDispatcher.setAccessChannel(channel_access);
        MainDispatcher.setFileChannel(channel_file);

        ServerCom<Package> server = new ServerCom<>(keystore, password, port);

        while (true) {
            try {
                Channel<Package> channel = server.processClient();
                new Handler<>(new ClientHandler(), channel).run();
            } catch (NoConnection noConnection) {
                noConnection.printStackTrace();
            }
        }

        //server.close();
    }

}

