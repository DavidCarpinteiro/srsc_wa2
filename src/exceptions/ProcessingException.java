package exceptions;

public class ProcessingException extends Exception {
    public ProcessingException() {
        super();
    }

    public ProcessingException(String message) {
        super(message);
    }
}
