package exceptions;

public class EncryptionError extends Exception {
    public EncryptionError() {
        super();
    }

    public EncryptionError(String msg) {
        super(msg);
    }
}
