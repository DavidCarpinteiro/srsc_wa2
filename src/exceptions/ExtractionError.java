package exceptions;

public class ExtractionError extends Exception {

    public ExtractionError() {
        super();
    }

    public ExtractionError(String message) {
        super(message);
    }
}
