package exceptions;

public class ChannelError extends Exception {
    public ChannelError() {
        super();
    }

    public ChannelError(String message) {
        super(message);
    }
}
