import communication.channel.Channel;
import communication.channel.ServerCom;
import communication.handler.Handler;
import communication.message.Package;
import exceptions.NoConnection;
import principals.server.user_authenticator.AuthHandler;
import principals.server.user_authenticator.FServerAuth;
import utils.CipherSuite;
import utils.Conf;

import java.security.KeyPair;

public class ServerAuthLauncher {
    public static void main(String[] args) {
        //System.setProperty("javax.net.debug", "ssl");

        System.out.println("Server Authenticator running...");

        String keystore = "configs/tls/authenticator.ks";
        String password = "authenticator";
        String trustStore = "configs/tls/authenticator.ts";
        String alias = "authenticator";
        String config_path = "configs/other/server.properties";

        int port = 1111;

        if (args.length == 6) {
            try {
                port = Integer.parseInt(args[0]);
                keystore = args[1];
                password = args[2];
                trustStore = args[3];
                alias = args[4];
                config_path = args[5];
            } catch (NumberFormatException ignored) {

            }

        }

        System.setProperty("javax.net.ssl.trustStore", trustStore);
        System.setProperty("javax.net.ssl.trustStorePassword", password);

        Conf.newInstance(config_path);

        ServerCom<Package> server = new ServerCom<>(keystore, password, port);

        KeyPair pair = CipherSuite.loadKeys(keystore, password, alias);

        FServerAuth.setKeyPair(pair);

        while (true) {
            try {
                Channel<Package> channel = server.processClient();
                new Handler<>(new AuthHandler(), channel).run();
            } catch (NoConnection e) {
                //e.printStackTrace();
                System.err.println("connection closed...");
            }
        }

        //server.close();
    }
}
