import communication.channel.Channel;
import communication.channel.ServerCom;
import communication.handler.Handler;
import communication.message.Package;
import exceptions.NoConnection;
import principals.server.access_controller.AccessControl;
import principals.server.access_controller.AccessHandler;
import utils.CipherSuite;
import utils.Conf;

import java.security.PublicKey;

public class ServerAccessCLauncher {
    public static void main(String[] args) {
        //System.setProperty("javax.net.debug", "ssl");

        System.out.println("Server Access Control running...");

        String keystore = "configs/tls/access.ks";
        String password = "access";
        String truststore = "configs/tls/access.ts";
        String alias = "access";
        String alias_of_other = "authenticator";
        String config_path = "configs/other/server.properties";

        int port = 2222;

        if (args.length == 7) {
            try {
                port = Integer.parseInt(args[0]);
                keystore = args[1];
                password = args[2];
                truststore = args[3];
                alias = args[4];
                alias_of_other = args[5];
                config_path = args[6];
            } catch (NumberFormatException ignored) {

            }
        }

        System.setProperty("javax.net.ssl.trustStore", truststore);
        System.setProperty("javax.net.ssl.trustStorePassword", password);

        Conf.newInstance(config_path);

        PublicKey key = CipherSuite.loadPublicKey(truststore, password, alias_of_other);

        AccessControl.setPublicKey(key);

        AccessControl.setKeyPair(CipherSuite.loadKeys(keystore, password, alias));

        ServerCom<Package> server = new ServerCom<>(keystore, password, port);

        while (true) {
            try {
                Channel<Package> channel = server.processClient();
                new Handler<>(new AccessHandler(), channel).run();
            } catch (NoConnection e) {
                //e.printStackTrace();
                System.err.println("connection closed...");
            }

        }

        //server.close();
    }
}
