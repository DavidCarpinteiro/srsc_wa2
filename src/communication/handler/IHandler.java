package communication.handler;

import exceptions.ProcessingException;

public interface IHandler<T> {

    T process(T data) throws ProcessingException;

}
