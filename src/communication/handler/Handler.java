package communication.handler;

import communication.channel.Channel;
import exceptions.ChannelError;
import exceptions.ProcessingException;
import exceptions.SocketClosed;

import java.io.Serializable;

public class Handler<T extends Serializable> implements Runnable {

    private final IHandler<T> handler;
    private final Channel<T> channel;

    public Handler(IHandler<T> handler, Channel<T> channel) {
        this.handler = handler;
        this.channel = channel;
    }

    @Override
    public void run() {
        while (true) {
            try {
                T data = channel.receive();
                T reply = handler.process(data);
                channel.send(reply);
            } catch (ChannelError | ProcessingException | SocketClosed e) {
                System.err.println("communication exception, socket closed");
                return;
            }
        }
    }
}
