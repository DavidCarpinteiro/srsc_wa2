package communication.token;

import java.util.Date;

public class TokenDecrypted implements IToken {
    private final String username;
    private final Date ttl;

    public TokenDecrypted(String username, Date ttl) {
        this.username = username;
        this.ttl = ttl;
    }

    public String getUsername() {
        return username;
    }

    public Date getTtl() {
        return ttl;
    }
}
