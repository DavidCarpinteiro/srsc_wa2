package communication.token;

public class TokenEncrypted implements IToken {

    private final String token;

    public TokenEncrypted(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
