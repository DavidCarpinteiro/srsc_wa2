package communication.channel;

import exceptions.NoConnection;
import utils.Conf;
import utils.Props;

import javax.net.ssl.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.ServerSocket;
import java.security.KeyStore;
import java.util.Objects;

public class ServerCom<T extends Serializable> {

    private ServerSocket server_socket;

    public ServerCom(String keystore, String password, int port) {
        Objects.requireNonNull(keystore);
        Objects.requireNonNull(password);

        char[] passwordC = password.toCharArray();

        String[] cipher_suites = Conf.getAll(Props.CIPHER_SUITES);
        String[] protocols = Conf.getAll(Props.PROTOCOLS);

        System.out.println("initializing at port " + port);

        try {
            SSLContext sc = SSLContext.getInstance(Conf.get(Props.SSL_CONTEXT));
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(Conf.get(Props.KEY_MANAGER_TYPE));
            KeyStore ks = KeyStore.getInstance(Conf.get(Props.KEYSTORE_TYPE));

            ks.load(new FileInputStream(keystore), passwordC);

            kmf.init(ks, passwordC);

            sc.init(kmf.getKeyManagers(), null, null);

            SSLServerSocketFactory ssf = sc.getServerSocketFactory();
            server_socket = ssf.createServerSocket(port);

            ((SSLServerSocket) server_socket).setEnabledProtocols(protocols);
            ((SSLServerSocket) server_socket).setEnabledCipherSuites(cipher_suites);

            ((SSLServerSocket) server_socket).setNeedClientAuth(true);

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("error instantiating server");
        }
    }

    public Channel<T> processClient() throws NoConnection {
        System.out.println("waiting for client");
        try {
            SSLSocket s = (SSLSocket) server_socket.accept();
            s.startHandshake();
            for (String a : s.getEnabledCipherSuites()) {
                System.out.println("Using Cipher Suite: " + a);
            }
            System.out.println("got new client");
            return new Channel<>(s);
        } catch (IOException e) {
            e.printStackTrace();
            throw new NoConnection();
        }
    }

    public void close() {
        try {
            server_socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Exited successfully");
        System.out.flush();
    }


}
