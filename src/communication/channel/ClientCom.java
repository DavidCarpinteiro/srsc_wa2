package communication.channel;

import utils.AuthType;
import utils.Conf;
import utils.Props;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.FileInputStream;
import java.io.Serializable;
import java.security.KeyStore;
import java.util.Objects;

public class ClientCom<T extends Serializable> {

    private final Channel<T> channel;

    private ClientCom(String keystore, String password, String ip, int port) {
        Objects.requireNonNull(ip);
        SSLSocket socket;

        String[] cipher_suites = Conf.getAll(Props.CIPHER_SUITES);
        String[] protocols = Conf.getAll(Props.PROTOCOLS);

        if (Conf.get(Props.TLS_AUTH_TYPE).equals(AuthType.MUTUAL.name())) {
            Objects.requireNonNull(keystore);
            Objects.requireNonNull(password);

            try {
                System.out.println("running mutual authentication with " + ip + "/" + port);
                char[] passwordC = password.toCharArray();

                SSLContext sc = SSLContext.getInstance(Conf.get(Props.SSL_CONTEXT));
                KeyManagerFactory kmf = KeyManagerFactory.getInstance(Conf.get(Props.KEY_MANAGER_TYPE));
                KeyStore ks = KeyStore.getInstance(Conf.get(Props.KEYSTORE_TYPE));

                ks.load(new FileInputStream(keystore), passwordC);

                kmf.init(ks, passwordC);

                sc.init(kmf.getKeyManagers(), null, null);

                SSLSocketFactory factory = sc.getSocketFactory();

                socket = (SSLSocket) factory.createSocket(ip, port);

                socket.setEnabledProtocols(protocols);
                socket.setEnabledCipherSuites(cipher_suites);

                socket.startHandshake();
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("could not instantiate client");
            }
        } else {
            System.out.println("running server only authentication");

            SSLSocketFactory f = (SSLSocketFactory) SSLSocketFactory.getDefault();
            try {
                socket = (SSLSocket) f.createSocket(ip, port);

                socket.setEnabledProtocols(protocols);
                socket.setEnabledCipherSuites(cipher_suites);

                socket.startHandshake();
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("could not instantiate client");
            }
        }

        channel = new Channel<>(socket);
    }

    public static <T extends Serializable> Channel<T> getChannel(String keystore, String password, String ip, int port) {
        return new ClientCom<T>(keystore, password, ip, port).channel;
    }

}
