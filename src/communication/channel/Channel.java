package communication.channel;

import exceptions.ChannelError;
import exceptions.SocketClosed;

import javax.net.ssl.SSLSocket;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Channel<T extends Serializable> {
    private final SSLSocket socket;
    private final ObjectOutputStream output;
    private final ObjectInputStream input;

    public Channel(SSLSocket socket) {
        try {
            this.socket = socket;
            output = new ObjectOutputStream(socket.getOutputStream());
            input = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("could not instantiate channel");
        }
    }

    public void send(T obj) throws SocketClosed, ChannelError {
        if (obj == null) throw new ChannelError("Null Object");

        if (!socket.isClosed() && socket.isConnected()) {
            try {
                output.writeObject(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            throw new SocketClosed();
        }
    }

    @SuppressWarnings("unchecked")
    public T receive() throws SocketClosed {
        if (!socket.isClosed() && socket.isConnected()) {
            try {
                T obj = (T) input.readObject();
                if (obj == null) throw new ChannelError("Null Reply");
                return obj;
            } catch (Exception e) {
                //e.printStackTrace();
                close();
                throw new SocketClosed();
            }
        } else {
            throw new SocketClosed();
        }
    }

    public void close() {
        try {
            socket.close();
            output.close();
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
