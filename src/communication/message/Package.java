package communication.message;


import communication.token.TokenEncrypted;
import exceptions.NoToken;

import java.io.Serializable;
import java.util.Objects;

public final class Package implements Serializable {
    private final Object type;
    private final Object data;
    private TokenEncrypted token;

    public Package(Object type, Object data) {
        Objects.requireNonNull(type);
        Objects.requireNonNull(data);

        this.type = type;
        this.data = data;
        this.token = null;
    }

    public Package(Object type, Object data, TokenEncrypted token) {
        Objects.requireNonNull(type);
        Objects.requireNonNull(data);
        Objects.requireNonNull(token);

        this.type = type;
        this.data = data;
        this.token = token;
    }

    @SuppressWarnings("unchecked")
    public final <E extends Enum> E getType() {
        return (E) type;
    }

    @SuppressWarnings("unchecked")
    public final <T> T getData() {
        return (T) data;
    }

    public TokenEncrypted getToken() throws NoToken {
        if (token == null) throw new NoToken();
        return token;
    }

    public void setToken(TokenEncrypted token) {
        Objects.requireNonNull(token);
        this.token = token;
    }
}
