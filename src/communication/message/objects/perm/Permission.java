package communication.message.objects.perm;

public enum Permission {
    READ,
    WRITE,
}
