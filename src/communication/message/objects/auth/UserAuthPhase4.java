package communication.message.objects.auth;

import communication.message.objects.IObjectData;

import java.util.Date;

public class UserAuthPhase4 implements IObjectData {
    private final String encrypted_content;


    public UserAuthPhase4(String encrypted_content) {
        this.encrypted_content = encrypted_content;
    }

    public String getEncrypted_content() {
        return encrypted_content;
    }

    public static class Content implements IObjectData {
        private final String username;
        private final String token;
        private final Date ttl;
        private final long random;

        public Content(String username, String token, Date ttl, long random) {
            this.username = username;
            this.token = token;
            this.ttl = ttl;
            this.random = random;
        }

        public String getUsername() {
            return username;
        }

        public String getToken() {
            return token;
        }

        public Date getTtl() {
            return ttl;
        }

        public long getRandom() {
            return random;
        }
    }
}


