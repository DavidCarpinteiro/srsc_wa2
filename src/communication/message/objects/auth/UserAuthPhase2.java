package communication.message.objects.auth;

import communication.message.objects.IObjectData;

public class UserAuthPhase2 implements IObjectData {
    private final long random;
    private final String key;

    public UserAuthPhase2(long random, String key) {
        this.random = random;
        this.key = key;
    }

    public long getRandom() {
        return random;
    }

    public String getKey() {
        return key;
    }
}
