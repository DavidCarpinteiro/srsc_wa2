package communication.message.objects.auth;

import communication.message.objects.IObjectData;

public class UserAuthPhase3 implements IObjectData {
    private final String signature;
    private final long random;
    private final String key;
    private final String username;

    public UserAuthPhase3(String signature, long random, String key, String username) {
        this.signature = signature;
        this.random = random;
        this.key = key;
        this.username = username;
    }

    public String getSignature() {
        return signature;
    }

    public long getRandom() {
        return random;
    }

    public String getKey() {
        return key;
    }

    public String getUsername() {
        return username;
    }

    public static class Content implements IObjectData {
        private final String hash_password;
        private final long random;

        public Content(String hash_password, long random) {
            this.hash_password = hash_password;
            this.random = random;
        }

        public String getHash_password() {
            return hash_password;
        }

        public long getRandom() {
            return random;
        }
    }
}
