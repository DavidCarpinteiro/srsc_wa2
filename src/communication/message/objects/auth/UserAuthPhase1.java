package communication.message.objects.auth;

import communication.message.objects.IObjectData;

public class UserAuthPhase1 implements IObjectData {
    private final String username;

    public UserAuthPhase1(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

}
