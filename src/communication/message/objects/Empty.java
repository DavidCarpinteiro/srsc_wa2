package communication.message.objects;

public class Empty implements IObjectData {

    private String message;

    public Empty(String message) {

        this.message = message;
    }

    public Empty() {
        this.message = "empty";
    }


    public String getMessage() {
        return message;
    }
}
