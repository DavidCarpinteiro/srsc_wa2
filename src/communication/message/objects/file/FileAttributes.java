package communication.message.objects.file;

import communication.message.objects.IObjectData;

import java.util.Date;

public class FileAttributes implements IObjectData {
    private final String name;
    private final boolean isDirectory;
    private final String type;
    private final Date creation;
    private final Date modification;

    public FileAttributes(String name, boolean isDirectory, String type, Date creation, Date modification) {
        this.name = name;
        this.isDirectory = isDirectory;
        this.type = type;
        this.creation = creation;
        this.modification = modification;
    }

    public String getName() {
        return name;
    }

    public boolean isDirectory() {
        return isDirectory;
    }

    public String getType() {
        return type;
    }

    public Date getCreation() {
        return creation;
    }

    public Date getModification() {
        return modification;
    }
}
