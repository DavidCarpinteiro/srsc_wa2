package communication.message.objects.file;

import communication.message.objects.IObjectData;

public class FileRequest implements IObjectData {

    private String username;
    private String path;
    private String path_to;
    private FileDataObject file;

    public FileRequest(String username, String path, FileDataObject file) {
        this.username = username;
        this.path = path;
        this.file = file;
    }

    public FileRequest(String username) {
        this.username = username;
        this.path = null;
        this.file = null;
    }

    public FileRequest(String username, String path) {
        this.username = username;
        this.path = path;
        this.file = null;
    }

    public FileRequest(String username, String path, String path_to) {
        this.username = username;
        this.path = path;
        this.path_to = path_to;
        this.file = null;
    }

    public String getUsername() {
        return username;
    }

    public String getPath() {
        return path;
    }

    public FileDataObject getFile() {
        return file;
    }

    public String getPath_to() {
        return path_to;
    }
}
