package communication.message.objects.file;

import communication.message.objects.IObjectData;

import java.util.Arrays;

public class FileDataObject implements IObjectData {

    private final byte[] data;
    private final byte[] signedHash;

    public FileDataObject(byte[] data, byte[] signedHash) {
        this.data = data;
        this.signedHash = signedHash;
    }

    public byte[] getData() {
        return this.data;
    }

    public byte[] getSignedHash() {
        return this.signedHash;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;

        if (!(o instanceof FileDataObject))
            return false;

        FileDataObject other = (FileDataObject) o;

        return Arrays.equals(this.getData(), other.getData()) && Arrays.equals(this.getSignedHash(), other.getSignedHash());
    }
}
