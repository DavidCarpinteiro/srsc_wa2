package communication.message.objects.file;

import java.io.Serializable;

public class FileDataBlock implements Serializable {

    private final byte[] data;
    private final int sizeWithoutPadding;
    private final String nextBlockName;
    private final boolean isTerminal;

    public FileDataBlock(byte[] data, int sizeWithoutPadding, String nextBlockName, boolean isTerminal) {
        this.data = data;
        this.sizeWithoutPadding = sizeWithoutPadding;
        this.nextBlockName = nextBlockName;
        this.isTerminal = isTerminal;
    }

    public byte[] getData() {
        return data;
    }

    public int getSizeWithoutPadding() {
        return sizeWithoutPadding;
    }

    public String getNextBlockName() {
        return nextBlockName;
    }

    public boolean isTerminal() {
        return isTerminal;
    }
}
