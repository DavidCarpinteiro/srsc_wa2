package communication.message.objects.file;

import java.io.Serializable;
import java.util.Date;

public class RemoteFileAttributes implements Serializable {

    private final Date creation;
    private final Date modification;
    private final boolean isDir;

    public RemoteFileAttributes(Date creation, Date modification, boolean isDir) {
        this.creation = creation;
        this.modification = modification;
        this.isDir = isDir;
    }

    public Date getModification() {
        return modification;
    }

    public Date getCreation() {
        return creation;
    }

    public boolean isDir() {
        return isDir;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;

        if (!(o instanceof RemoteFileAttributes))
            return false;

        RemoteFileAttributes other = (RemoteFileAttributes) o;

        return this.isDir == other.isDir;
    }
}
