package communication.message.type;

public enum MainType {
    EXEC_AUTHENTICATION,
    EXEC_FILE_OPERATION,
    FAIL
}
