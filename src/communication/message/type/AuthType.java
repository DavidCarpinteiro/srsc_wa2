package communication.message.type;

public enum AuthType {
    PHASE_1,
    PHASE_2,
    PHASE_3,
    PHASE_4,
}
