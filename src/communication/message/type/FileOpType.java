package communication.message.type;

public enum FileOpType {
    LS,
    LS_PATH,
    MK_DIR,
    PUT,
    GET,
    CP,
    RM,
    RM_DIR,
    FILE,

    OK,
    NOT_OK
}
