package communication.message.type;

public enum AccessCType {
    VERIFY_ACCESS,
    OK,
    NOT_OK
}
