package principals.server.access_controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import communication.message.objects.perm.Permission;
import communication.token.TokenDecrypted;
import communication.token.TokenEncrypted;
import exceptions.EncryptionError;
import exceptions.ExtractionError;
import utils.CipherSuite;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyPair;
import java.security.PublicKey;
import java.util.*;

public class AccessControl {
    private static final Type TYPE = new TypeToken<Map<String, List<Permission>>>() {
    }.getType();
    private static final String dir = System.getProperty("user.dir") + "/tables/access/data.txt";
    private static PublicKey key;
    private static KeyPair pair;

    static {
        saveDataToFile();
    }

    private static Map<String, List<Permission>> getDataFromFile() {
        Path path = Paths.get(dir);
        try {
            String data = new String(Files.readAllBytes(path));
            return new Gson().fromJson(data, TYPE);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Could not load file");
        }
    }

    private static void saveDataToFile() {
        Map<String, List<Permission>> permissions = new HashMap<>(1);
        permissions.put("test", Arrays.asList(Permission.READ, Permission.WRITE));
        permissions.put("readUser", Collections.singletonList(Permission.READ));

        String data = new Gson().toJson(permissions);

        File file = new File(dir);
        try (BufferedWriter output = new BufferedWriter(new FileWriter(file))) {
            output.write(data);
            output.flush();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Could not save file");
        }

    }

    public static boolean checkPermission(String username, Permission p) {
        return getDataFromFile().get(username).contains(p);
    }

    public static KeyPair getKeyPair() {
        return pair;
    }

    public static void setKeyPair(KeyPair keyPair) {
        pair = keyPair;
    }

    public static PublicKey getPublicKey() {
        return key;
    }

    public static void setPublicKey(PublicKey publicKey) {
        key = publicKey;
    }

    public static TokenDecrypted checkToken(TokenEncrypted token) throws ExtractionError {

        if (token == null) {
            System.err.println("Token is null");
            throw new ExtractionError();
        }

        String token_dec = null;
        try {
            token_dec = CipherSuite.decryptAsymmetric(token.getToken(), key);
        } catch (EncryptionError encryptionError) {
            throw new ExtractionError(encryptionError.getMessage());
        }

        if (token_dec == null) {
            System.err.println("Could not decrypt");
            throw new ExtractionError();
        }

        try {
            TokenDecrypted dec = (TokenDecrypted) CipherSuite.deserialize(token_dec);

            if (dec.getTtl().before(new Date())) {
                System.err.println("TTL is expired");
                throw new ExtractionError();
            }

            return dec;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new ExtractionError();
        }
    }
}
