package principals.server.access_controller;

import communication.handler.IHandler;
import communication.message.Package;
import communication.message.objects.Empty;
import communication.message.objects.perm.Permission;
import communication.message.type.AccessCType;
import communication.token.TokenDecrypted;
import communication.token.TokenEncrypted;
import utils.CipherSuite;

import java.util.Date;

public class AccessHandler implements IHandler<Package> {

    @Override
    public Package process(Package message) {
        try {
            AccessCType type = message.getType();

            switch (type) {
                case VERIFY_ACCESS:
                    Permission perm = message.getData();
                    TokenEncrypted token_enc = message.getToken();

                    String token_string = CipherSuite.decryptAsymmetric(token_enc.getToken(), AccessControl.getPublicKey());
                    TokenDecrypted token = (TokenDecrypted) CipherSuite.deserialize(token_string);

                    if (token.getTtl().before(new Date())) {
                        System.err.println("Expired TTL");
                        return new Package(AccessCType.NOT_OK, new Empty("expired ttl"));
                    }
                    String username = token.getUsername();

                    if (!AccessControl.checkPermission(username, perm)) {
                        System.err.println("No permissions");
                        return new Package(AccessCType.NOT_OK, new Empty("no permission"));
                    }

                    String enc = CipherSuite.encryptAsymmetric(token_string, AccessControl.getKeyPair().getPrivate());
                    token_enc = new TokenEncrypted(enc);

                    System.out.println("User " + username + " has valid access");
                    return new Package(AccessCType.OK, new Empty("validated"), token_enc);
                default:
                    System.err.println("Wrong type in Message: " + type);
                    return new Package(AccessCType.OK, new Empty("wrong type in Message: " + type));
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Could not extract message");
            return new Package(AccessCType.OK, new Empty("could not extract message"));
        }
    }

}
