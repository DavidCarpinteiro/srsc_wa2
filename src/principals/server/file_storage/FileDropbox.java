package principals.server.file_storage;

import com.dropbox.core.DbxDownloader;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.*;
import communication.message.objects.file.FileDataObject;
import communication.message.objects.file.RemoteFileAttributes;
import exceptions.NoConnection;
import principals.client.FileManager;
import utils.CipherSuite;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystemException;
import java.nio.file.NotDirectoryException;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class FileDropbox implements FileStorage {

    // TODO load access token from file?
    private static final String ACCESS_TOKEN = "kkVYeEMzJgAAAAAAAAAADBZlfNKuzIxlmItWsBx7IJZh-Bnze0cfe4YCEAKtFwD2";

    // Dropbox imposes a limit of 150 MB on a single uploaded file; so everything to be uploaded must be chunked
    private static final int CHUNK_SIZE = 100;

    private final DbxClientV2 client;

    public FileDropbox() {
        // Create Dropbox client
        DbxRequestConfig config = DbxRequestConfig.newBuilder("srsc/wa2").build();
        client = new DbxClientV2(config, ACCESS_TOKEN);
    }

    private void checkConnection() throws NoConnection {
        try {
            client.users().getCurrentAccount();
        } catch (DbxException e) {
            throw new NoConnection();
        }
    }

    @Override
    public List<String> ls(String username) throws NotDirectoryException, NoConnection {
        checkConnection();
        return ls(username, "");
    }

    @Override
    public List<String> ls(String username, String path) throws NotDirectoryException, NoConnection {
        checkConnection();
        List<String> contents = new LinkedList<>();

        try {
            // Get files and folder metadata from Dropbox directory
            ListFolderResult result = client.files().listFolder("/" + username + path);
            while (true) {
                for (Metadata metadata : result.getEntries()) {
                    contents.add(metadata.getName());
                }

                if (!result.getHasMore()) {
                    break;
                }

                result = client.files().listFolderContinue(result.getCursor());
            }
        } catch (DbxException e) {
            throw new NotDirectoryException(e.getMessage());
        }

        return contents;
    }

    @Override
    public void mkdir(String username, String path) throws NotDirectoryException, NoConnection {
        checkConnection();
        try {
            client.files().createFolderV2("/" + username + path);
        } catch (DbxException e) {
            throw new NotDirectoryException(e.getMessage());
        }
    }

    @Override
    public void put(String username, String path, FileDataObject file) throws FileSystemException, NoConnection {
        checkConnection();
        try {
            // Serialize the file object, convert the result to bytes and break thosew bytes into chunks
            Iterator<byte[]> chunks = FileManager.splitIntoChunksOfSize(CipherSuite.toBytes(CipherSuite.serialize(file)), CHUNK_SIZE).iterator();

            // Start Dropbox upload session
            UploadSessionStartUploader startUploader = client.files().uploadSessionStart();

            // Get first chunk
            byte[] chunk = chunks.next();

            // Write first chunk to session stream
            startUploader.getOutputStream().write(chunk);

            // Finish this upload request and get the session ID
            String sessionId = startUploader.finish().getSessionId();

            // Create a cursor that tracks the session; second arg is the amount of data uploaded in  the entire session
            UploadSessionCursor cursor = new UploadSessionCursor(sessionId, chunk.length);

            // Close this upload request
            startUploader.close();

            // For every chunk append it to the session
            while (chunks.hasNext()) {
                // Get next chunk
                chunk = chunks.next();

                // Append chunk to session at cursor position
                UploadSessionAppendV2Uploader appendV2Uploader = client.files().uploadSessionAppendV2(cursor, false);

                // Write chunk
                appendV2Uploader.getOutputStream().write(chunk);

                // Finish and close this upload request
                appendV2Uploader.finish();
                appendV2Uploader.close();

                // Update cursor
                cursor = new UploadSessionCursor(sessionId, cursor.getOffset() + chunk.length);
            }

            // Finish the session completely
            UploadSessionFinishUploader uploadFinisher = client.files().uploadSessionFinish(cursor, new CommitInfo("/" + username + path));
            uploadFinisher.finish();
            uploadFinisher.close();

        } catch (IOException | DbxException e) {
            throw new FileSystemException("Could not create file: " + "/" + username + path);
        }
    }

    @Override
    public FileDataObject get(String username, String path_file) throws FileNotFoundException, NoConnection {
        checkConnection();
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            // Get downloader
            DbxDownloader<FileMetadata> downloader = client.files().download("/" + username + path_file);

            // Download file into output stream
            downloader.download(outputStream);

            // Convert from bytes to FileDataObject
            FileDataObject file = (FileDataObject) CipherSuite.deserialize(CipherSuite.toBase64(outputStream.toByteArray()));

            // Close the downloader and finish
            downloader.close();

            return file;
        } catch (DbxException | IOException | ClassNotFoundException e) {
            throw new FileNotFoundException(e.getMessage());
        }
    }

    @Override
    public void cp(String username, String path_file_orig, String path_file_dest) throws FileNotFoundException, NoConnection {
        checkConnection();
        try {
            client.files().copyV2("/" + username + path_file_orig, "/" + username + path_file_dest);
        } catch (DbxException e) {
            throw new FileNotFoundException(e.getMessage());
        }
    }

    @Override
    public void rm(String username, String path_file) throws NotDirectoryException, FileNotFoundException, NoConnection {
        checkConnection();
        try {
            String fullPath = "/" + username + path_file;

            // Check if element in path is folder or file
            Metadata data = client.files().getMetadata(fullPath);
            if (data instanceof FolderMetadata) {
                throw new NotDirectoryException(fullPath);
            }

            client.files().deleteV2(fullPath);
        } catch (DbxException e) {
            throw new FileNotFoundException(e.getMessage());
        }
    }

    @Override
    public void rmdir(String username, String path) throws NotDirectoryException, NoConnection {
        checkConnection();
        try {
            String fullPath = "/" + username + path;

            // Check if element in path is folder or file
            Metadata data = client.files().getMetadata(fullPath);

            if (data instanceof FileMetadata) {
                throw new NotDirectoryException(fullPath);
            }

            client.files().deleteV2(fullPath);
        } catch (DbxException e) {
            throw new NotDirectoryException(path);
        }
    }

    @Override
    public RemoteFileAttributes file(String username, String path) throws FileNotFoundException, NoConnection {
        checkConnection();
        /*
            Dropbox doesn't allow for the checking of creation time; only last modification time.
         */
        try {
            String fullPath = "/" + username + path;
            Metadata metadata = client.files().getMetadata(fullPath);
            Date lastModified;
            boolean isDir;

            if (metadata instanceof FileMetadata) {
                lastModified = ((FileMetadata) metadata).getServerModified();
                isDir = false;
            } else {
                /*
                    For some reason, Dropbox does not actually offer a way to get a folder's modification time.
                    Since something must be returned, and null doesn't really make sense and might break functionality; a new Date (now) is returned.
                 */
                lastModified = new Date();
                isDir = true;
            }

            return new RemoteFileAttributes(lastModified, lastModified, isDir);

        } catch (DbxException e) {
            throw new FileNotFoundException(e.getMessage());
        }
    }
}
