package principals.server.file_storage;

import communication.message.objects.file.FileDataObject;
import communication.message.objects.file.RemoteFileAttributes;
import communication.token.TokenDecrypted;
import communication.token.TokenEncrypted;
import exceptions.EncryptionError;
import exceptions.ExtractionError;
import exceptions.NoConnection;
import utils.CipherSuite;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystemException;
import java.nio.file.NotDirectoryException;
import java.security.KeyPair;
import java.security.PublicKey;
import java.util.*;

public class FileProxy {

    private static PublicKey key;
    private static KeyPair pair;

    private static Set<FileStorage> storageReplicas;

    static {
        storageReplicas = new HashSet<>();

        storageReplicas.add(new LocalStorage());
        storageReplicas.add(new FileDropbox());
    }

    private static void removeReplicas(List<FileStorage> toRemove) {
        for (FileStorage fileStorage : toRemove)
            storageReplicas.remove(fileStorage);

        if (storageReplicas.size() == 0) {
            System.err.println("NO MORE WORKING REPLICAS");
            System.exit(-1);
        }
    }

    public static List<String> ls(String username) throws NotDirectoryException {
        return ls(username, "");
    }

    public static List<String> ls(String username, String path) throws NotDirectoryException {
        // List where non-responding replicas go, waiting to be removed from all replicas
        List<FileStorage> toRemove = new LinkedList<>();

        // Save replicas results; only need to maintain the first, because they will all be equal; if not then straight fail-stop everything;
        Set<String> prevResult = null;

        // Iterate all storage replicas
        for (FileStorage replica : storageReplicas) {
            Set<String> replicaRes;
            try {
                // Get command result for current replica
                replicaRes = new HashSet<>(replica.ls(username, path));
            } catch (NoConnection noConnection) {
                // NoConnectionException means this replica must be removed
                toRemove.add(replica);
                continue;
            }

            // Check if current result is coherent with previous one
            if (prevResult != null && !replicaRes.equals(prevResult)) {
                System.err.println("CONFLICTING RESULTS FROM STORAGE REPLICAS ON LS COMMAND");
                System.exit(-1);
            }

            prevResult = replicaRes;
        }

        removeReplicas(toRemove);

        return new LinkedList<>(prevResult);
    }

    public static void mkdir(String username, String path) throws NotDirectoryException {
        // List where non-responding replicas go, waiting to be removed from all replicas
        List<FileStorage> toRemove = new LinkedList<>();

        for (FileStorage replica : storageReplicas) {
            try {
                replica.mkdir(username, path);
            } catch (NoConnection noConnection) {
                toRemove.add(replica);
            }
        }

        removeReplicas(toRemove);
    }

    public static void put(String username, String path, FileDataObject file) throws FileSystemException {
        // List where non-responding replicas go, waiting to be removed from all replicas
        List<FileStorage> toRemove = new LinkedList<>();

        for (FileStorage replica : storageReplicas) {
            try {
                replica.put(username, path, file);
            } catch (NoConnection noConnection) {
                toRemove.add(replica);
            }
        }

        removeReplicas(toRemove);
    }

    public static FileDataObject get(String username, String path_file) throws FileNotFoundException {
        // List where non-responding replicas go, waiting to be removed from all replicas
        List<FileStorage> toRemove = new LinkedList<>();

        // Save replicas results; only need to maintain the first, because they will all be equal; if not then straight fail-stop everything;
        FileDataObject prevResult = null;

        // Iterate all storage replicas
        for (FileStorage replica : storageReplicas) {
            FileDataObject replicaRes;
            try {
                // Get command result for current replica
                replicaRes = replica.get(username, path_file);
            } catch (NoConnection noConnection) {
                // NoConnectionException means this replica must be removed
                toRemove.add(replica);
                continue;
            }

            // Check if current result is coherent with previous one
            if (prevResult != null && !replicaRes.equals(prevResult)) {
                System.err.println("CONFLICTING RESULTS FROM STORAGE REPLICAS ON GET COMMAND");
                System.exit(-1);
            }

            prevResult = replicaRes;
        }

        removeReplicas(toRemove);

        return prevResult;
    }

    public static void cp(String username, String path_file_orig, String path_file_dest) throws FileNotFoundException, FileSystemException {
        // List where non-responding replicas go, waiting to be removed from all replicas
        List<FileStorage> toRemove = new LinkedList<>();

        for (FileStorage replica : storageReplicas) {
            try {
                replica.cp(username, path_file_orig, path_file_dest);
            } catch (NoConnection noConnection) {
                toRemove.add(replica);
            }
        }

        removeReplicas(toRemove);
    }

    public static void rm(String username, String path_file) throws NotDirectoryException, FileNotFoundException {
        // List where non-responding replicas go, waiting to be removed from all replicas
        List<FileStorage> toRemove = new LinkedList<>();

        for (FileStorage replica : storageReplicas) {
            try {
                replica.rm(username, path_file);
            } catch (NoConnection noConnection) {
                toRemove.add(replica);
            }
        }

        removeReplicas(toRemove);
    }

    public static void rmdir(String username, String path) throws NotDirectoryException {
        // List where non-responding replicas go, waiting to be removed from all replicas
        List<FileStorage> toRemove = new LinkedList<>();

        for (FileStorage replica : storageReplicas) {
            try {
                replica.rmdir(username, path);
            } catch (NoConnection noConnection) {
                toRemove.add(replica);
            }
        }

        removeReplicas(toRemove);
    }

    public static RemoteFileAttributes file(String username, String path) throws FileNotFoundException {
        // List where non-responding replicas go, waiting to be removed from all replicas
        List<FileStorage> toRemove = new LinkedList<>();

        // Save replicas results; only need to maintain the first, because they will all be equal; if not then straight fail-stop everything;
        RemoteFileAttributes prevResult = null;

        // Iterate all storage replicas
        for (FileStorage replica : storageReplicas) {
            RemoteFileAttributes replicaRes;
            try {
                // Get command result for current replica
                replicaRes = replica.file(username, path);
            } catch (NoConnection noConnection) {
                // NoConnectionException means this replica must be removed
                toRemove.add(replica);
                continue;
            }

            // Check if current result is coherent with previous one
            if (prevResult != null && !replicaRes.equals(prevResult)) {
                System.err.println("CONFLICTING RESULTS FROM STORAGE REPLICAS ON FILE COMMAND");
                System.exit(-1);
            }

            prevResult = replicaRes;
        }

        removeReplicas(toRemove);

        return prevResult;
    }

    public static KeyPair getKeyPair() {
        return pair;
    }

    public static void setKeyPair(KeyPair keyPair) {
        pair = keyPair;
    }

    public static PublicKey getPublicKey() {
        return key;
    }

    public static void setPublicKey(PublicKey publicKey) {
        key = publicKey;
    }

    public static TokenDecrypted checkToken(TokenEncrypted token) throws ExtractionError {

        if (token == null) {
            System.err.println("Token is null");
            throw new ExtractionError();
        }

        String token_dec = null;
        try {
            token_dec = CipherSuite.decryptAsymmetric(token.getToken(), key);
        } catch (EncryptionError encryptionError) {
            throw new ExtractionError(encryptionError.getMessage());
        }

        if (token_dec == null) {
            System.err.println("Could not decrypt");
            throw new ExtractionError();
        }

        try {
            TokenDecrypted dec = (TokenDecrypted) CipherSuite.deserialize(token_dec);

            if (dec.getTtl().before(new Date())) {
                System.err.println("TTL is expired");
                throw new ExtractionError();
            }

            return dec;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new ExtractionError();
        }
    }

}
