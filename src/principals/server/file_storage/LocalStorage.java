package principals.server.file_storage;

import communication.message.objects.file.FileDataObject;
import communication.message.objects.file.RemoteFileAttributes;

import java.io.*;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.NotDirectoryException;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class LocalStorage implements FileStorage {


    private final String pathPrefix = System.getProperty("user.dir") + "/tables/storage/";

    public LocalStorage() {
        this.setupStorage();
    }

    private void setupStorage() {
        File index = new File(pathPrefix);
        String[] entries = index.list();
        if (entries == null) return;
        try {
            for (String s : entries) {
                File currentFile = new File(index.getPath(), s);
                currentFile.delete();
            }
            // Add initial userspaces for testing
            if (!new File(pathPrefix + "test").mkdir()) {
                throw new RuntimeException("could not create directory");
            }
            if (!new File(pathPrefix + "readUser").mkdir()) {
                throw new RuntimeException("could not create directory");
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("could not delete files");
        }
    }

    @Override
    public List<String> ls(String username) throws NotDirectoryException {
        return ls(username, "");
    }

    @Override
    public List<String> ls(String username, String path) throws NotDirectoryException {
        List<String> fileNames = new LinkedList<>();
        File dir = new File(pathPrefix + username + path);

        if (!dir.isDirectory()) {
            throw new NotDirectoryException("Not a directory: " + pathPrefix + username + path);
        }

        if (dir.listFiles() != null)
            for (File file : dir.listFiles())
                fileNames.add(file.getName());

        return fileNames;
    }

    @Override
    public void mkdir(String username, String path) throws NotDirectoryException {
        if (!new File(pathPrefix + username + path).mkdir()) throw new NotDirectoryException(username + path);
    }

    @Override
    public void put(String username, String path, FileDataObject file) throws FileSystemException {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(
                new File(pathPrefix + username + path)))) {
            out.writeObject(file);
        } catch (IOException e) {
            throw new FileSystemException("Could not create file: " + pathPrefix + username + path);
        }
    }

    @Override
    public FileDataObject get(String username, String path_file) throws FileNotFoundException {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(new File(pathPrefix + username + path_file)))) {
            return (FileDataObject) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new FileNotFoundException("No file at path: " + pathPrefix + username + path_file);
        }
    }

    @Override
    public void cp(String username, String path_file_orig, String path_file_dest) throws FileNotFoundException, FileSystemException {
        put(username, path_file_dest, get(username, path_file_orig));
    }

    @Override
    public void rm(String username, String path_file) throws NotDirectoryException, FileNotFoundException {
        File file = new File(pathPrefix + username + path_file);
        if (file.isDirectory())
            throw new NotDirectoryException(username + path_file);
        if (!file.delete())
            throw new FileNotFoundException(username + path_file);
    }

    @Override
    public void rmdir(String username, String path) throws NotDirectoryException {
        File dir = new File(pathPrefix + username + path);
        if (!dir.isDirectory())
            throw new NotDirectoryException(username + path);
        if (!dir.delete()) {
            throw new NotDirectoryException(username + path);
        }
    }

    @Override
    public RemoteFileAttributes file(String username, String path) throws FileNotFoundException {
        try {
            File file = new File(pathPrefix + username + path);
            BasicFileAttributes basicAttributes = Files.readAttributes(Paths.get(pathPrefix + username + path), BasicFileAttributes.class);
            Date creationDate = new Date(basicAttributes.creationTime().toMillis());
            Date modifiedDate = new Date(basicAttributes.lastModifiedTime().toMillis());

            return new RemoteFileAttributes(creationDate, modifiedDate, file.isDirectory());
        } catch (IOException e) {
            throw new FileNotFoundException("No file at: " + pathPrefix + username + path);
        }
    }
}
