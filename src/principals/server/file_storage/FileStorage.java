package principals.server.file_storage;

import communication.message.objects.file.FileDataObject;
import communication.message.objects.file.RemoteFileAttributes;
import exceptions.NoConnection;

import java.io.FileNotFoundException;
import java.nio.file.FileSystemException;
import java.nio.file.NotDirectoryException;
import java.util.List;

public interface FileStorage {
    List<String> ls(String username) throws NotDirectoryException, NoConnection;

    List<String> ls(String username, String path) throws NotDirectoryException, NoConnection;

    void mkdir(String username, String path) throws NotDirectoryException, NoConnection;

    void put(String username, String path, FileDataObject file) throws FileSystemException, NoConnection;

    FileDataObject get(String username, String path_file) throws FileNotFoundException, NoConnection;

    void cp(String username, String path_file_orig, String path_file_dest) throws FileNotFoundException, FileSystemException, NoConnection;

    void rm(String username, String path_file) throws NotDirectoryException, FileNotFoundException, NoConnection;

    void rmdir(String username, String path) throws NotDirectoryException, NoConnection;

    RemoteFileAttributes file(String username, String path) throws FileNotFoundException, NoConnection;
}
