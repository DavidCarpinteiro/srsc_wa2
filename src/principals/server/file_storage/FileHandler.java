package principals.server.file_storage;

import communication.handler.IHandler;
import communication.message.Package;
import communication.message.objects.Empty;
import communication.message.objects.file.FileDataObject;
import communication.message.objects.file.FileRequest;
import communication.message.objects.file.RemoteFileAttributes;
import communication.message.type.FileOpType;
import communication.message.type.MainType;
import communication.token.TokenDecrypted;
import communication.token.TokenEncrypted;
import utils.CipherSuite;

import java.io.FileNotFoundException;
import java.nio.file.FileSystemException;
import java.nio.file.NotDirectoryException;
import java.util.List;

public class FileHandler implements IHandler<Package> {

    @Override
    public Package process(Package message) {

        try {
            FileOpType type = message.getType();
            FileRequest fileReq = message.getData();

            TokenEncrypted token_enc = message.getToken();
            TokenDecrypted token = FileProxy.checkToken(token_enc);

            String token_string = CipherSuite.decryptAsymmetric(token_enc.getToken(), FileProxy.getPublicKey());

            String enc = CipherSuite.encryptAsymmetric(token_string, FileProxy.getKeyPair().getPrivate());
            token_enc = new TokenEncrypted(enc);

            switch (type) {
                case LS:
                    System.out.println("Operation LS");
                    List<String> lsList = FileProxy.ls(fileReq.getUsername());
                    return new Package(FileOpType.OK, lsList, token_enc);
                case GET:
                    System.out.println("Operation GET");
                    FileDataObject file = FileProxy.get(fileReq.getUsername(), fileReq.getPath());
                    return new Package(FileOpType.OK, file, token_enc);
                case FILE:
                    System.out.println("Operation FILE");
                    RemoteFileAttributes fileAtt = FileProxy.file(fileReq.getUsername(), fileReq.getPath());
                    return new Package(FileOpType.OK, fileAtt, token_enc);
                case LS_PATH:
                    System.out.println("Operation LS PATH");
                    lsList = FileProxy.ls(fileReq.getUsername(), fileReq.getPath());
                    return new Package(FileOpType.OK, lsList, token_enc);
                case CP:
                    System.out.println("Operation CP");
                    FileProxy.cp(fileReq.getUsername(), fileReq.getPath(), fileReq.getPath_to());
                    return new Package(FileOpType.OK, true, token_enc);
                case RM:
                    System.out.println("Operation RM");
                    FileProxy.rm(fileReq.getUsername(), fileReq.getPath());
                    return new Package(FileOpType.OK, true, token_enc);
                case PUT:
                    System.out.println("Operation PUT");
                    FileProxy.put(fileReq.getUsername(), fileReq.getPath(), fileReq.getFile());
                    return new Package(FileOpType.OK, true, token_enc);
                case MK_DIR:
                    System.out.println("Operation MK_DIR");
                    FileProxy.mkdir(fileReq.getUsername(), fileReq.getPath());
                    return new Package(FileOpType.OK, true, token_enc);
                case RM_DIR:
                    System.out.println("Operation RM_DIR");
                    FileProxy.rmdir(fileReq.getUsername(), fileReq.getPath());
                    return new Package(FileOpType.OK, true, token_enc);
                default:
                    System.err.println("Wrong type in Message: " + type);
                    return new Package(MainType.FAIL, new Empty("wrong type in Message: " + type));
            }

        } catch (FileNotFoundException e) {
            return new Package(FileOpType.NOT_OK, new Empty("file not found"));
        } catch (NotDirectoryException e) {
            return new Package(FileOpType.NOT_OK, new Empty("not a directory"));
        } catch (FileSystemException e) {
            return new Package(FileOpType.NOT_OK, new Empty("error creating file"));
        } catch (Exception e) {
            System.err.println("Could not extract message: " + e.getMessage());
            return new Package(MainType.FAIL, new Empty("could not extract message"));
        }
    }
}
