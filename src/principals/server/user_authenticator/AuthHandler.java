package principals.server.user_authenticator;

import communication.handler.IHandler;
import communication.message.Package;
import communication.message.objects.Empty;
import communication.message.objects.auth.UserAuthPhase1;
import communication.message.objects.auth.UserAuthPhase2;
import communication.message.objects.auth.UserAuthPhase3;
import communication.message.objects.auth.UserAuthPhase4;
import communication.message.type.AuthType;
import communication.message.type.MainType;
import communication.token.TokenDecrypted;
import exceptions.ProcessingException;
import utils.CipherSuite;
import utils.Conf;
import utils.DiffieHellman;
import utils.Props;

import java.security.PublicKey;
import java.util.Calendar;
import java.util.Date;

public class AuthHandler implements IHandler<Package> {

    @Override
    public Package process(Package message) throws ProcessingException {

        try {
            AuthType type = message.getType();

            switch (type) {
                case PHASE_1:
                    UserAuthPhase1 phase1 = message.getData();

                    if (!FServerAuth.userExists(phase1.getUsername())) {
                        System.err.println("No username found");
                        return new Package(MainType.FAIL, new Empty("invalid credentials"));
                    }

                    if (FServerAuth.tempAuthExists(phase1.getUsername())) {
                        System.err.println("Already ongoing auth");
                        return new Package(MainType.FAIL, new Empty("invalid sequence of operation"));
                    }

                    DiffieHellman dh = new DiffieHellman();

                    long rd = CipherSuite.getRandom();

                    UserData userData = FServerAuth.getUserData(phase1.getUsername());

                    if (userData.isBlocked()) {
                        System.err.println("User is locked");
                        return new Package(MainType.FAIL, new Empty("locked user"));
                    }

                    FServerAuth.addTempAuth(phase1.getUsername(), dh, rd + 1);
                    PublicKey publicKey = dh.getPublic();
                    String publicKeyString = CipherSuite.getString(publicKey);
                    String publicKeyStringEnc = CipherSuite.encryptPBE(publicKeyString, userData.getPassword(), rd);

                    UserAuthPhase2 phase2 = new UserAuthPhase2(rd, publicKeyStringEnc);
                    Package pkg = new Package(AuthType.PHASE_2, phase2);

                    System.out.println("User " + phase1.getUsername() + " has initiated authentication process");

                    return pkg;
                case PHASE_3:
                    UserAuthPhase3 phase3 = message.getData();

                    if (!FServerAuth.tempAuthExists(phase3.getUsername())) {
                        System.err.println("No ongoing auth");
                        return new Package(MainType.FAIL, new Empty("invalid credentials"));
                    }

                    FServerAuth.DiffieRandom dr = FServerAuth.getDiffie(phase3.getUsername());
                    dh = dr.diffieHellman;

                    publicKeyStringEnc = phase3.getKey();
                    publicKeyString = CipherSuite.decryptPBE(publicKeyStringEnc, FServerAuth.getUserData(phase3.getUsername()).getPassword(), phase3.getRandom());
                    publicKey = CipherSuite.getKey(publicKeyString);
                    dh.doFinal(publicKey);
                    String sign = dh.decrypt(phase3.getSignature());
                    UserAuthPhase3.Content content = (UserAuthPhase3.Content) CipherSuite.deserialize(sign);

                    if (dr.rd != content.getRandom()) {
                        System.err.println("Random does not match");
                        return new Package(MainType.FAIL, new Empty("invalid credentials"));
                    }
                    String hash_pass = FServerAuth.getUserData(phase3.getUsername()).getPassword();

                    if (!hash_pass.equals(content.getHash_password())) {
                        System.err.println("Password does not match");
                        return new Package(MainType.FAIL, new Empty("invalid credentials"));
                    }

                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(new Date());
                    calendar.add(Calendar.HOUR_OF_DAY, Integer.parseInt(Conf.get(Props.TOKEN_TTL)));
                    TokenDecrypted token_dec = new TokenDecrypted(phase3.getUsername(), calendar.getTime());
                    String token_string = CipherSuite.serialize(token_dec);
                    String token_enc = CipherSuite.encryptAsymmetric(token_string, FServerAuth.getKeyPair().getPrivate());

                    UserAuthPhase4.Content cont = new UserAuthPhase4.Content(
                            phase3.getUsername(), token_enc, calendar.getTime(), phase3.getRandom() + 1
                    );
                    String con_string = CipherSuite.serialize(cont);
                    String con_enc = dh.encrypt(con_string);
                    UserAuthPhase4 phase4 = new UserAuthPhase4(con_enc);
                    pkg = new Package(AuthType.PHASE_4, phase4);

                    FServerAuth.removeTempAuth(phase3.getUsername());

                    System.out.println("User " + phase3.getUsername() + " has been successful authenticated");
                    return pkg;
                default:
                    System.err.println("Wrong type in Message: " + type);
                    return new Package(MainType.FAIL, new Empty("wrong type in Message: " + type));
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Could not extract message");
            return new Package(MainType.FAIL, new Empty("could not extract message"));
        }


    }


}
