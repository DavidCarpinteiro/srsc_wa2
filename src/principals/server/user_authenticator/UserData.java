package principals.server.user_authenticator;

public class UserData {
    private final String username;
    private final String email;
    private final String name;
    private final String password;
    private final boolean blocked;

    public UserData(String username, String email, String name, String password) {
        this.username = username;
        this.email = email;
        this.name = name;
        this.password = password;
        this.blocked = false;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public boolean isBlocked() {
        return blocked;
    }
}