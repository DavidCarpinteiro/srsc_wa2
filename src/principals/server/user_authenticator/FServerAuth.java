package principals.server.user_authenticator;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import utils.DiffieHellman;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyPair;
import java.util.HashMap;
import java.util.Map;

public class FServerAuth {

    private static final Type TYPE = new TypeToken<Map<String, UserData>>() {
    }.getType();
    private static final String dir = System.getProperty("user.dir") + "/tables/auth/data.txt";
    private static Map<String, DiffieRandom> temp_auth;
    private static KeyPair keyPair;

    static {
        temp_auth = new HashMap<>(10);

        //Test file
        saveDataToFile();
    }

    private static Map<String, UserData> getDataFromFile() {
        Path path = Paths.get(dir);
        try {
            String data = new String(Files.readAllBytes(path));
            return new Gson().fromJson(data, TYPE);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Could not load file");
        }
    }

    private static void saveDataToFile() {
        Map<String, UserData> user_data = new HashMap<>();
        user_data.put("test", new UserData("test", "test", "test", "ZheqiKcua1JriMvO2jiKe1Kg6FYUihLZuEKc0qU6PqQ="));
        user_data.put("readUser", new UserData("readUser", "readUser", "readUser", "ZheqiKcua1JriMvO2jiKe1Kg6FYUihLZuEKc0qU6PqQ="));

        String data = new Gson().toJson(user_data);

        File file = new File(dir);
        try (BufferedWriter output = new BufferedWriter(new FileWriter(file))) {
            output.write(data);
            output.flush();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Could not save file");
        }

    }

    public static KeyPair getKeyPair() {
        return keyPair;
    }

    public static void setKeyPair(KeyPair key) {
        keyPair = key;
    }

    public static UserData getUserData(String username) {
        return getDataFromFile().get(username);
    }

    public static boolean userExists(String username) {
        return getDataFromFile().containsKey(username);
    }

    public static void addTempAuth(String username, DiffieHellman dh, long rd) {
        temp_auth.put(username, new DiffieRandom(dh, rd));
    }

    public static void removeTempAuth(String username) {
        temp_auth.remove(username);
    }

    public static boolean tempAuthExists(String username) {
        return temp_auth.containsKey(username);
    }

    public static DiffieRandom getDiffie(String username) {
        return temp_auth.get(username);
    }

    static class DiffieRandom {
        DiffieHellman diffieHellman;
        long rd;

        public DiffieRandom(DiffieHellman dh, long rd) {
            this.diffieHellman = dh;
            this.rd = rd;
        }
    }
}


