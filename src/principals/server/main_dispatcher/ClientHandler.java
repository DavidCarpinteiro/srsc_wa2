package principals.server.main_dispatcher;

import communication.handler.IHandler;
import communication.message.Package;
import communication.message.objects.Empty;
import communication.message.objects.perm.Permission;
import communication.message.type.AccessCType;
import communication.message.type.MainType;
import communication.token.TokenDecrypted;
import communication.token.TokenEncrypted;

public class ClientHandler implements IHandler<Package> {


    @Override
    public Package process(Package data) {

        MainType type = data.getType();

        try {
            switch (type) {
                case EXEC_AUTHENTICATION:
                    return MainDispatcher.authenticate(data);
                case EXEC_FILE_OPERATION:
                    //Get decrypted TokenDecrypted
                    TokenDecrypted token = MainDispatcher.checkToken(data.getToken());

                    //Check Access Control
                    Package tmp = data.getData();
                    Permission perm = MainDispatcher.getRequiredPerm(tmp.getType());
                    Package to_access = new Package(AccessCType.VERIFY_ACCESS, perm, data.getToken());
                    Package access = MainDispatcher.authorize(to_access);

                    if (access.getType() != AccessCType.OK) {
                        System.err.println("Type does not match: " + access.getType());
                        return new Package(MainType.FAIL, new Empty("wrong type in Message: " + type));
                    }

                    //Send Request to File Server
                    TokenEncrypted for_file = access.getToken();
                    tmp.setToken(for_file);

                    return MainDispatcher.fileRequest(tmp);
                default:
                    System.err.println("Wrong type in Message: " + type);
                    return new Package(MainType.FAIL, new Empty("wrong type in Message: " + type));
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("Could not extract message");
            return new Package(MainType.FAIL, new Empty("could not extract message"));
        }
    }
}
