package principals.server.main_dispatcher;

import communication.channel.Channel;
import communication.message.Package;
import communication.message.objects.perm.Permission;
import communication.message.type.FileOpType;
import communication.token.TokenDecrypted;
import communication.token.TokenEncrypted;
import exceptions.ChannelError;
import exceptions.EncryptionError;
import exceptions.ExtractionError;
import exceptions.SocketClosed;
import utils.CipherSuite;

import java.io.IOException;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class MainDispatcher {

    private static Channel<Package> authChannel, accessChannel, fileChannel;
    private static PublicKey key;

    private static List<FileOpType> read = Arrays.asList(
            FileOpType.FILE, FileOpType.GET, FileOpType.LS,
            FileOpType.LS_PATH
    );

    public static void setAuthChannel(Channel<Package> auth_channel) {
        MainDispatcher.authChannel = auth_channel;
    }

    public static void setAccessChannel(Channel<Package> access_channel) {
        MainDispatcher.accessChannel = access_channel;
    }

    public static void setFileChannel(Channel<Package> file_channel) {
        MainDispatcher.fileChannel = file_channel;
    }

    public static void setPublicKey(PublicKey publicKey) {
        key = publicKey;
    }

    public static TokenDecrypted checkToken(TokenEncrypted token) throws ExtractionError {

        if (token == null) {
            System.err.println("Token is null");
            throw new ExtractionError();
        }
        String token_dec = null;
        try {
            token_dec = CipherSuite.decryptAsymmetric(token.getToken(), key);
        } catch (EncryptionError encryptionError) {
            throw new ExtractionError(encryptionError.getMessage());
        }

        if (token_dec == null) {
            System.err.println("Could not decrypt");
            throw new ExtractionError();
        }

        try {
            TokenDecrypted dec = (TokenDecrypted) CipherSuite.deserialize(token_dec);

            if (dec.getTtl().before(new Date())) {
                System.err.println("TTL is expired");
                throw new ExtractionError();
            }

            return dec;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new ExtractionError();
        }
    }

    public static Permission getRequiredPerm(FileOpType type) {
        if (read.contains(type))
            return Permission.READ;
        else
            return Permission.WRITE;
    }


    public static Package authenticate(Package data) throws ChannelError {
        try {
            authChannel.send(data.getData());
            return authChannel.receive();
        } catch (SocketClosed | ChannelError e) {
            e.printStackTrace();
            throw new ChannelError();
        }
    }

    public static Package authorize(Package data) throws ChannelError {
        try {
            accessChannel.send(data);
            return accessChannel.receive();
        } catch (SocketClosed | ChannelError e) {
            e.printStackTrace();
            throw new ChannelError();
        }
    }

    public static Package fileRequest(Package data) throws ChannelError {
        try {
            fileChannel.send(data);
            return fileChannel.receive();
        } catch (SocketClosed e) {
            throw new ChannelError();
        }
    }
}
