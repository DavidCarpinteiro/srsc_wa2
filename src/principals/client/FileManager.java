package principals.client;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import communication.message.objects.file.FileAttributes;
import communication.message.objects.file.FileDataBlock;
import communication.message.objects.file.FileDataObject;
import communication.message.objects.file.RemoteFileAttributes;
import exceptions.EncryptionError;
import utils.CipherSuite;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.*;

public class FileManager {

    /*
        Base64 encoding includes the '/' character used in UNIX style paths; this is an inconvenience when a base64 string is to be used in a filepath.
        The solution chosen to circumvent this problem was to replace all '/' characters by '_', which shouldn't have any adverse effects in a filepath.
        Must remember to do the opposite replacement when decoding.
     */
    private static final String BASE64_SLASH_STAND_IN = "_";
    private final KeyPair keyPair;
    private final String cryptoAlg;
    private final String keyAlg;
    private Map<String, byte[]> pathHashKeyMap;
    private Map<String, String> pathHashToPathMap;
    private static final byte PAD_ELEMENT = 0;
    private final int blockSize;
    private Set<String> allKnownPaths;

    public FileManager(String keystorePath, String keystorePassword, String alias, String cryptoAlg, String keyAlg, int blockSize) {
        this.pathHashKeyMap = new HashMap<>(100);
        this.pathHashToPathMap = new HashMap<>(100);
        this.cryptoAlg = cryptoAlg;
        this.keyAlg = keyAlg;

        this.keyPair = CipherSuite.loadKeys(keystorePath, keystorePassword, alias);

        this.blockSize = blockSize;
        this.allKnownPaths = new HashSet<>(100);
    }

    private String encrypt(String value, byte[] keyBytes) throws EncryptionError {
        SecretKeySpec aesKey = new SecretKeySpec(keyBytes, 0, 16, keyAlg);

        try {
            Cipher cipher = Cipher.getInstance(cryptoAlg);
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);

            return CipherSuite.toBase64(cipher.doFinal(CipherSuite.toBytes(value)));
        } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new EncryptionError();
        }

    }

    private String decrypt(String value, byte[] keyBytes) throws EncryptionError {
        SecretKeySpec aesKey = new SecretKeySpec(keyBytes, 0, 16, keyAlg);

        try {
            Cipher cipher = Cipher.getInstance(cryptoAlg);
            cipher.init(Cipher.DECRYPT_MODE, aesKey);

            return CipherSuite.toBase64(cipher.doFinal(CipherSuite.toBytes(value)));
        } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new EncryptionError();
        }
    }

    private byte[] generateKeyBytes() {
        byte[] keyBytes = new byte[256];
        new SecureRandom().nextBytes(keyBytes);
        return keyBytes;
    }

    public List<String> decryptFilenames(List<String> encryptedFilenames, String pathPrefix) throws EncryptionError {

        // Hash path prefix
        String encPathPrefix = getEncryptedPath(pathPrefix);

        // Convert given list of encrypted filenames into unencrypted filenames
        List<String> decryptedFilenames = new LinkedList<>();

        //TODO fix for case where encryptedFilenames is null
        //if (encryptedFilenames == null) return decryptedFilenames;

        for (String encFilename : encryptedFilenames) {
            String plainPath = this.pathHashToPathMap.get(encPathPrefix + "/" + encFilename);
            if (plainPath != null) {
                //TODO fix for path having /tmp/*
                //plainPath.replace("/tmp", "");
                decryptedFilenames.add(plainPath.substring(plainPath.lastIndexOf("/") + 1));
            }
        }
        return decryptedFilenames;
    }

    public String getEncryptedPath(String plainPath) throws EncryptionError {
        // Encrypt every path component in the given path
        String[] pathComponents = plainPath.split("/");
        StringBuilder stringBuilder = new StringBuilder();

        for (String pathComponent : pathComponents) {
            // Only append if a path component  is not an empty string;
            if (!pathComponent.isEmpty()) {
                String encodedString = CipherSuite.digest(CipherSuite.toBase64(pathComponent.getBytes()));

                // Make the base64 string safe, i.e. replace all slashes
                String encodedSafeString = encodedString.replaceAll("/", BASE64_SLASH_STAND_IN);

                stringBuilder.append("/").append(encodedSafeString);
            }
        }
        return stringBuilder.toString();
    }

    public FileAttributes buildFileAttributes(String path, RemoteFileAttributes remoteFileAttributes) throws EncryptionError {
        String name = this.pathHashToPathMap.get(getEncryptedPath(path)).substring(path.lastIndexOf("/") + 1);
        String type;
        if (remoteFileAttributes.isDir())
            type = "dir";
        else {
            int dotIndex = name.lastIndexOf(".");
            type = dotIndex == -1 ? "file" : name.substring(dotIndex + 1);
        }
        return new FileAttributes(name, remoteFileAttributes.isDir(), type, remoteFileAttributes.getCreation(), remoteFileAttributes.getModification());
    }

    public FileDataObject createSecureFile(String path, FileDataBlock fileDataBlock) throws FileAlreadyExistsException, EncryptionError {
        // Digest path
        String pathHash = getEncryptedPath(path);

        // Check if digested path already exists
        if (this.pathHashKeyMap.containsKey(pathHash))
            throw new FileAlreadyExistsException("secure version of " + path + " already exists");

        // Generate new symmetric key
        byte[] keyBytes = generateKeyBytes();
        this.pathHashKeyMap.put(pathHash, keyBytes);

        // Covert binary file data to base&4 string
        String fileDataString = null;
        try {
            fileDataString = CipherSuite.serialize(fileDataBlock);
        } catch (IOException e) {
            throw new EncryptionError("error when serializing data block");
        }

        // Encrypt file
        String encryptedFileString = encrypt(fileDataString, keyBytes);

        // Create file hash and sign it with private key
        String signedFileHash = CipherSuite.encryptAsymmetric(CipherSuite.digest(encryptedFileString), this.keyPair.getPublic());

        // Convert data content and signed digest to bytes; encapsulate in data object and return;
        return new FileDataObject(CipherSuite.toBytes(encryptedFileString), CipherSuite.toBytes(signedFileHash));
    }

    public FileDataBlock extractSecureFile(String path, FileDataObject secureFile) throws EncryptionError, FileNotFoundException {
        // Get encrypted data in string format
        String encryptedDataString = CipherSuite.toBase64(secureFile.getData());

        // Get signed hash in string format
        String signedHashString = CipherSuite.toBase64(secureFile.getSignedHash());

        // Get recorded hash of encrypted data; decrypting with private key
        String hashString = CipherSuite.decryptAsymmetric(signedHashString, this.keyPair.getPrivate());

        // Compute hash of the encrypted data;
        String digest = CipherSuite.digest(encryptedDataString);

        // Compare actual hash with recorded expected hash
        if (!digest.equals(hashString))
            throw new EncryptionError("recorded file hash does not match actual hash");

        // Get the symmetric key necessary to decrypt the data
        byte[] keyBytes = this.pathHashKeyMap.get(getEncryptedPath(path));
        if (keyBytes == null)
            throw new FileNotFoundException("No entry for " + path + " in file manager");

        // Get decrypted data in string format;
        String decryptedDataString = decrypt(encryptedDataString, keyBytes);

        // Return decrypted data in byte form
        FileDataBlock block = null;
        try {
            block = (FileDataBlock) CipherSuite.deserialize(decryptedDataString);
        } catch (IOException | ClassNotFoundException e) {
            throw new EncryptionError("error deserializing data block");
        }
        return block;
    }

    public void addKnownPath(String path) throws EncryptionError {
        this.allKnownPaths.add(getEncryptedPath(path));
    }

    public void removeKnownPath(String path) throws EncryptionError {
        this.allKnownPaths.remove(getEncryptedPath(path));
    }

    public void addPathAssociation(String path) throws EncryptionError {
        this.pathHashToPathMap.put(getEncryptedPath(path), path);
        this.allKnownPaths.add(path);
    }

    public void removePathAssociation(String path) throws EncryptionError {
        this.pathHashToPathMap.remove(getEncryptedPath(path));
        this.allKnownPaths.remove(path);
    }

    public static List<byte[]> splitIntoChunksOfSize(byte[] completeData, int blockSize) {
        // Get the number of dat chunks that will have size equal to block size
        int numChunks = completeData.length / blockSize;

        // Get the number of leftover bytes, if any
        int leftoverBytes = completeData.length % blockSize;

        List<byte[]> chunks = new ArrayList<>(numChunks + (leftoverBytes > 0 ? 1 : 0));

        // Split data into whole chunks
        int i = 0;
        while (i < numChunks) {
            chunks.add(Arrays.copyOfRange(completeData, i * blockSize, i * blockSize + blockSize));
            i++;
        }

        // If needed add last smaller chunk
        if (leftoverBytes > 0) {
            chunks.add(Arrays.copyOfRange(completeData, i * blockSize, completeData.length));
        }

        return chunks;
    }

    public List<byte[]> splitIntoChunks(byte[] completeData) {
        return splitIntoChunksOfSize(completeData, blockSize);
    }

    public String getRandomFreeFileName(String path) throws EncryptionError {
        String prefix = path.substring(0, path.lastIndexOf("/"));
        SecureRandom secRand = new SecureRandom();
        byte[] randomFilename = new byte[16];

        String newPath;
        String encryptedPath;
        int i = 0;
        while (i < 1000) {
            secRand.nextBytes(randomFilename);
            newPath = prefix + "/" + CipherSuite.toBase64(randomFilename).replaceAll("/", BASE64_SLASH_STAND_IN);
            encryptedPath = getEncryptedPath(newPath);

            if (!allKnownPaths.contains(encryptedPath))
                return newPath;

            i++;
        }
        throw new EncryptionError("Could not generate valid random path");
    }

    public FileDataBlock makeDataBlock(byte[] chunk, String nextFilename, boolean isFinal) {

        byte[] data = chunk;
        int sizeWithoutPadding = chunk.length;
        String next = nextFilename;

        // if this is to be a terminal block, must check to pad and mark ending on nextBlock;
        if (isFinal) {
            if (chunk.length < blockSize) {
                data = new byte[blockSize];
                System.arraycopy(chunk, 0, data, 0, chunk.length);
                Arrays.fill(data, chunk.length, data.length, PAD_ELEMENT);
            }
            next = null;
        }


        return new FileDataBlock(data, sizeWithoutPadding, next, isFinal);
    }

    public byte[] assembleDataFromBlocks(List<FileDataBlock> blocks) {
        ByteOutputStream output = new ByteOutputStream();

        for (FileDataBlock block : blocks) {
            output.write(block.getData(), 0, block.getSizeWithoutPadding());
        }

        byte[] res = new byte[output.getCount()];
        System.arraycopy(output.getBytes(), 0, res, 0, output.getCount());

        output.close();

        return res;
    }
}
