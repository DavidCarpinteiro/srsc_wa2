package principals.client;

import communication.channel.Channel;
import communication.message.Package;
import communication.message.objects.Empty;
import communication.message.objects.auth.UserAuthPhase1;
import communication.message.objects.auth.UserAuthPhase2;
import communication.message.objects.auth.UserAuthPhase3;
import communication.message.objects.auth.UserAuthPhase4;
import communication.message.objects.file.*;
import communication.message.type.AuthType;
import communication.message.type.FileOpType;
import communication.message.type.MainType;
import communication.token.TokenEncrypted;
import exceptions.ChannelError;
import exceptions.EncryptionError;
import exceptions.SocketClosed;
import utils.CipherSuite;
import utils.DiffieHellman;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.InvalidPathException;
import java.security.PublicKey;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Client {

    private final String username;
    private final Channel<Package> channel;
    private DiffieHellman dh;
    private String token;

    private FileManager fileManager;

    public Client(String username, Channel<Package> channel, FileManager fileManager) {
        this.username = username;
        this.channel = channel;
        this.fileManager = fileManager;
    }

    public boolean login(String password) {
        try {
            // Phase 1
            UserAuthPhase1 phase1 = new UserAuthPhase1(username);
            Package for_auth = new Package(AuthType.PHASE_1, phase1);
            Package pkg = new Package(MainType.EXEC_AUTHENTICATION, for_auth);
            channel.send(pkg);

            // Phase 2
            Package reply = channel.receive();

            pkg = reply;

            if (pkg.getType() != AuthType.PHASE_2) {
                System.err.println("Type is not Phase 2: " + pkg.getType());
                System.err.println(((Empty) pkg.getData()).getMessage());
                return false;
            }

            UserAuthPhase2 phase2 = pkg.getData();
            long rd = phase2.getRandom();
            String pass_hash = CipherSuite.digest(password);
            String pub_key_enc = phase2.getKey();

            String pub_key = CipherSuite.decryptPBE(pub_key_enc, pass_hash, rd);
            PublicKey keyP = CipherSuite.getKey(pub_key);
            dh = new DiffieHellman(keyP);

            long new_rd = CipherSuite.getRandom();
            PublicKey key = dh.getPublic();
            String key_s = CipherSuite.getString(key);
            String key_s_enc = CipherSuite.encryptPBE(key_s, pass_hash, new_rd);

            // Phase 3
            UserAuthPhase3.Content content = new UserAuthPhase3.Content(pass_hash, rd + 1);
            String content_string = CipherSuite.serialize(content);
            String content_enc = dh.encrypt(content_string);
            UserAuthPhase3 phase3 = new UserAuthPhase3(content_enc, new_rd, key_s_enc, username);
            for_auth = new Package(AuthType.PHASE_3, phase3);
            pkg = new Package(MainType.EXEC_AUTHENTICATION, for_auth);
            channel.send(pkg);
            // Phase 4
            reply = channel.receive();

            pkg = reply;

            if (pkg.getType() != AuthType.PHASE_4) {
                System.err.println("Type is not Phase 4");
                System.err.println(((Empty) pkg.getData()).getMessage());
                return false;
            }

            UserAuthPhase4 phase4 = pkg.getData();

            String enc = phase4.getEncrypted_content();
            String dec = dh.decrypt(enc);
            UserAuthPhase4.Content content4 = (UserAuthPhase4.Content) CipherSuite.deserialize(dec);

            if (content4.getRandom() != new_rd + 1) {
                System.err.println("Random does not match");
                System.err.println(((Empty) pkg.getData()).getMessage());
                return false;
            }

            if (!content4.getUsername().equals(username)) {
                System.err.println("Username does not match");
                System.err.println(((Empty) pkg.getData()).getMessage());
                return false;
            }

            token = content4.getToken();

            return true;
        } catch (SocketClosed | ChannelError | EncryptionError | IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<String> ls() throws EncryptionError {
        FileRequest req = new FileRequest(username);
        Package for_file = new Package(FileOpType.LS, req);
        List<String> res = exec(for_file);
        return this.fileManager.decryptFilenames(res, "/");
    }

    public List<String> ls(String path) throws EncryptionError {
        String encPath = fileManager.getEncryptedPath(path);
        FileRequest req = new FileRequest(username, encPath);
        Package for_file = new Package(FileOpType.LS_PATH, req);
        List<String> res = exec(for_file);
        if (res == null) throw new InvalidPathException(path, "RES IS NULL IN LS PATH");
        return fileManager.decryptFilenames(res, path);
    }

    public boolean mkdir(String path) throws EncryptionError {
        String encPath = fileManager.getEncryptedPath(path);
        FileRequest req = new FileRequest(username, encPath);
        Package for_file = new Package(FileOpType.MK_DIR, req);
        Boolean res = exec(for_file);
        if (res != null) {
            fileManager.addPathAssociation(path);
            return res;
        } else
            return false;
    }

    private boolean putBlock(String path, FileDataObject secureFile) throws EncryptionError {
        String encPath = fileManager.getEncryptedPath(path);
        FileRequest req = new FileRequest(username, encPath, secureFile);
        Package for_file = new Package(FileOpType.PUT, req);
        Boolean res = exec(for_file);
        if (res != null) {
            fileManager.addKnownPath(path);
            return res;
        } else
            return false;
    }

    public boolean put(String path, byte[] fileData) throws FileAlreadyExistsException, EncryptionError {
        // Split all data into chunks
        List<byte[]> chunks = fileManager.splitIntoChunks(fileData);

        fileManager.addPathAssociation(path);

        // For all the chunks, make them into blocks, encapsulate them in fileDataObjects and send them
        String currPath = path;
        String nextPath = fileManager.getRandomFreeFileName(path);

        // Iterate all chunks to convert them to blocks and send them to server
        Iterator<byte[]> chunkIterator = chunks.iterator();
        while (chunkIterator.hasNext()) {
            byte[] chunk = chunkIterator.next();

            FileDataBlock dataBlock;
            // If this is not the last chunk
            if (chunkIterator.hasNext()) {
                dataBlock = fileManager.makeDataBlock(chunk, nextPath, false);
            } else {
                dataBlock = fileManager.makeDataBlock(chunk, null, true);
            }

            FileDataObject secureFile = fileManager.createSecureFile(currPath, dataBlock);

            if (!putBlock(currPath, secureFile)) {
                return false;
            }

            currPath = nextPath;
            nextPath = fileManager.getRandomFreeFileName(currPath);
        }
        return true;
    }

    private FileDataBlock getBlock(String path_file) throws FileNotFoundException, EncryptionError {
        String encPath = fileManager.getEncryptedPath(path_file);
        FileRequest req = new FileRequest(username, encPath);
        Package for_file = new Package(FileOpType.GET, req);
        FileDataObject res = exec(for_file);
        if (res == null) throw new FileNotFoundException("RES IS NULL IN SERVER");
        return this.fileManager.extractSecureFile(path_file, res);
    }

    public byte[] get(String path_file) throws FileNotFoundException, EncryptionError {
        List<FileDataBlock> blocks = new LinkedList<>();
        FileDataBlock block;
        String nextBlockPath = path_file;

        do {
            block = getBlock(nextBlockPath);
            nextBlockPath = block.getNextBlockName();
            blocks.add(block);
        } while (!block.isTerminal());

        return fileManager.assembleDataFromBlocks(blocks);
    }

    public boolean cp(String path_file_orig, String path_file_dest) throws EncryptionError {
        try {
            byte[] origFile = get(path_file_orig);
            put(path_file_dest, origFile);
        } catch (FileNotFoundException | FileAlreadyExistsException e) {
            return false;
        }
        fileManager.addPathAssociation(path_file_dest);
        return true;
    }

    private boolean rmBlock(String path_file) throws EncryptionError {
        String encPath = fileManager.getEncryptedPath(path_file);
        FileRequest req = new FileRequest(username, encPath);
        Package for_file = new Package(FileOpType.RM, req);
        Boolean res = exec(for_file);
        if (res != null) {
            fileManager.removeKnownPath(path_file);
            return res;
        } else
            return false;
    }

    public boolean rm(String path_file) throws EncryptionError {

        String currPath = path_file;
        FileDataBlock block;
        do {
            try {
                block = getBlock(currPath);
            } catch (FileNotFoundException e) {
                return false;
            }
            if (!rmBlock(currPath)) {
                return false;
            }
            currPath = block.getNextBlockName();
        } while (!block.isTerminal());

        fileManager.removePathAssociation(path_file);
        return true;
    }

    public boolean rmdir(String path) throws EncryptionError {
        String encPath = fileManager.getEncryptedPath(path);
        FileRequest req = new FileRequest(username, encPath);
        Package for_file = new Package(FileOpType.RM_DIR, req);
        Boolean res = exec(for_file);
        if (res != null) {
            fileManager.removePathAssociation(path);
            return res;
        } else
            return false;
    }

    public FileAttributes file(String path) throws FileNotFoundException, EncryptionError {
        String encPath = fileManager.getEncryptedPath(path);
        FileRequest req = new FileRequest(username, encPath);
        Package for_file = new Package(FileOpType.FILE, req);
        RemoteFileAttributes res = exec(for_file);
        if (res == null) throw new FileNotFoundException();
        return fileManager.buildFileAttributes(path, res);
    }

    protected <T> T exec(Package for_file) {
        TokenEncrypted t = new TokenEncrypted(token);
        Package pkg = new Package(MainType.EXEC_FILE_OPERATION, for_file, t);
        try {
            channel.send(pkg);
            pkg = channel.receive();

            if (pkg.getType() == FileOpType.NOT_OK) {
                System.err.println(((Empty) pkg.getData()).getMessage());
                return null;
            } else if (pkg.getType() == FileOpType.OK) {
                return pkg.getData();
            } else {
                System.err.println("Wrong type: " + pkg.getType());
                System.err.println(((Empty) pkg.getData()).getMessage());
                return null;
            }
        } catch (SocketClosed | ChannelError e) {
            e.printStackTrace();
            return null;
        }
    }


}
