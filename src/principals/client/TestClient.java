package principals.client;

import communication.channel.Channel;
import communication.message.Package;
import communication.message.objects.Empty;
import communication.message.type.FileOpType;
import communication.message.type.MainType;
import communication.token.TokenEncrypted;
import exceptions.ChannelError;
import exceptions.SocketClosed;

public class TestClient extends Client {

    private final Channel<Package> testChannel;
    private final String fakeToken;


    public TestClient(String username, Channel<Package> channel, FileManager fileManager, String fakeToken) {
        super(username, channel, fileManager);
        this.fakeToken = fakeToken;
        this.testChannel = channel;
    }

    @Override
    protected <T> T exec(Package for_file) {
        TokenEncrypted t = new TokenEncrypted(fakeToken);
        Package pkg = new Package(MainType.EXEC_FILE_OPERATION, for_file, t);
        try {
            testChannel.send(pkg);
            pkg = testChannel.receive();

            if (pkg.getType() == FileOpType.NOT_OK) {
                System.err.println(((Empty) pkg.getData()).getMessage());
                return null;
            } else if (pkg.getType() == FileOpType.OK) {
                return pkg.getData();
            } else {
                System.err.println("Wrong type: " + pkg.getType());
                System.err.println(((Empty) pkg.getData()).getMessage());
                return null;
            }
        } catch (SocketClosed | ChannelError e) {
            e.printStackTrace();
            return null;
        }
    }
}