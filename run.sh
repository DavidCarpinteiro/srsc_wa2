#!/bin/bash

docker kill auth
docker kill acesscontrol
docker kill file
docker kill dispatcher

echo "Run all containers in the same terminal window? (y||n)"
read same_ter

if [[ "$same_ter" == "y" ]]
then
    docker start auth
    docker start acesscontrol
    docker start file
else
    gnome-terminal --geometry 60x20+0+0 -e "docker start -a auth"
    gnome-terminal --geometry 60x20+610+0 -e "docker start -a acesscontrol"
    gnome-terminal --geometry 60x20+1220+0 -e "docker start -a file"
fi

# Needed to give time for the others to launch
sleep 5

docker start -a dispatcher